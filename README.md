# Banky

## Quick start

- Make sure your NodeJS and npm versions are up to date for `React 16.8.6`

- Install dependencies: `npm install` or `yarn`

- Start the server: `npm run start` or `yarn start`

- Views are on: `localhost:3000`

### formatting

- if you are using vscode, install extension called "prettier eslint" and adjust these in settings.json:

```
// .vscode/settings.json
{
    "files.associations": {
        "\*.jsx": "javascriptreact"
    },
    "editor.insertSpaces": true,
    "editor.detectIndentation": false,
    "editor.formatOnSave": true,
    "editor.codeActionsOnSave": {
        "source.fixAll.eslint": true
    }
}
```

## State Management

We use Redux and Redux-Thunk. the store is in `src/store` and you can add a new store there and add it to `index.ts`

Available stores:

    - wallet: get available active wallet lists

    - account: login, logout, crud on profile

## File Structure

Within the download you'll find the following directories and files:

---

## Generating Code:


To create a new template:

``` hygen generator new --name YOUR_TEMPLATE_NAME ```

To generate from a template:

``` hygen YOUR_TEMPLATE_NAME new --PARAM_1_KEY PARAM_1_VALUE --PARAM_2_KEY PARAM_2_VALUE ```

---

## Feature List Plan:

- Services:

  - Cash in/out

    - [x] Route

    - [x] Form

    - Form Validation with errors

    - State management

- User Management:

  - Listing with:

    - Filtration
    - Sorting with [joinedDate,lastActive]

- Home Page

  - Settlement Report

  - Transaction List component

  - State management

- Components Interface:

  - [x] Col

  - [x] Row

  - [x] Page

  - [x] Text Field

  - [x] Select Field
