import 'react-perfect-scrollbar/dist/css/styles.css';
import React, { Suspense } from 'react';
import { useRoutes } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/core';
import GlobalStyles from 'shared/components/GlobalStyles';
import 'mixins/chartjs';
import theme from 'config/styling/theme';
import routes from 'pages/routes';
import { Provider } from 'react-redux';
import Spinner from 'shared/components/spinner';
import { store } from './store';

const App = () => {
  const routing = useRoutes(routes);

  return (
    <Suspense fallback={<Spinner loading={true} />}>
      <ThemeProvider theme={theme}>
        <GlobalStyles />
        <Provider store={store}>{routing}</Provider>
      </ThemeProvider>
    </Suspense>
  );
};

export default App;
