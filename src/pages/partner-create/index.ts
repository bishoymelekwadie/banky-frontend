import { connect } from 'react-redux';
import { StoreReducersList } from 'shared/types';
import { cleanStore, createItem } from 'store/partner/actionCreators';
import CreatePartner from './CreatePartner';

const mapStateToProps = (state: StoreReducersList) => ({
  userDetails: state.account.userData,
  isLoading: state.partner.isCreateItemLoading,
  createdItem: state.partner.createdItem,
  createItemErr: state.partner.createItemErr,
});

export default connect(mapStateToProps, (dispatch: any) => ({
  createItem: ({ userId, userRole, item }: any) =>
    dispatch(createItem({ userId, userRole, item })),
  cleanStore: () => dispatch(cleanStore()),
}))(CreatePartner);
