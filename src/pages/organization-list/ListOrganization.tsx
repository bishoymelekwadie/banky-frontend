import React from 'react';
import { Container } from '@material-ui/core';
import Page from 'shared/components/Page';
import Row from 'shared/components/Row';
import Col from 'shared/components/Col';
import OrganizationTable from 'shared/components/organization-table';
import Spinner from 'shared/components/spinner';

interface ListOrganizationProps {
  isLoading: boolean;
}

const ListOrganization = (props: ListOrganizationProps) => {
  const { isLoading } = props;

  return (
    <Page className="mt-8" title="Organization List">
      <Spinner loading={isLoading} />
      <Container maxWidth={false}>
        <Row>
          <Col className="mt-4" xs={12}>
            <OrganizationTable />
          </Col>
        </Row>
      </Container>
    </Page>
  );
};

export default ListOrganization;
