import { connect } from 'react-redux';
import { StoreReducersList } from 'shared/types';
import ListOrganization from './ListOrganization';

const mapStateToProps = (state: StoreReducersList) => ({
  isLoading: state.organization.isGetListLoading,
});

export default connect(mapStateToProps)(ListOrganization);
