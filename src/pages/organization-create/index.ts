import { connect } from 'react-redux';
import { StoreReducersList } from 'shared/types';
import { cleanStore, createItem } from 'store/organization/actionCreators';
import CreateOrganization from './CreateOrganization';

const mapStateToProps = (state: StoreReducersList) => ({
  userDetails: state.account.userData,
  isLoading: state.organization.isCreateItemLoading,
  createdItem: state.organization.createdItem,
  createItemErr: state.organization.createItemErr,
});

export default connect(mapStateToProps, (dispatch: any) => ({
  createItem: ({ userId, userRole, item }: any) =>
    dispatch(createItem({ userId, userRole, item })),
  cleanStore: () => dispatch(cleanStore()),
}))(CreateOrganization);
