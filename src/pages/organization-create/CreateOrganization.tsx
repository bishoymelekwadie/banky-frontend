import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Card, CardHeader, Container, Divider } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import * as Yup from 'yup';
import { Formik } from 'formik';
import Page from 'shared/components/Page';
import Row from 'shared/components/Row';
import Col from 'shared/components/Col';
import TextField, { TextFieldType } from 'shared/components/form/TextField';
import Alert, { AlertTypes } from 'shared/components/Alert';
import Button from 'shared/components/Button';
import Spinner from 'shared/components/spinner';
import { Organization } from 'store/api';

interface CreateOrganizationProps {
  createdItem: any;
  createItemErr: string;
  userDetails: any;
  isLoading: boolean;
  createItem: any;
  cleanStore: any;
}

const initialFormValues = {
  commercial_contact_email: '',
  commercial_contact_name: '',
  commercial_contact_phone: '',
  corporate_address: '',
  corporate_email: '',
  corporate_phone: '',
  name: '',
  type: Organization.TypeEnum.CioPartener,
};

const CreateOrganization = (props: CreateOrganizationProps) => {
  const {
    createdItem,
    createItem,
    createItemErr,
    userDetails,
    isLoading,
  } = props;
  const { t } = useTranslation();
  const navigate = useNavigate();

  useEffect(() => {
    return props.cleanStore;
  }, []);

  return (
    <Page className="mt-8" title={t('organization.create.title')}>
      <Spinner loading={isLoading} />
      <Container maxWidth={false}>
        <Row>
          <Col className="mt-4" xs={12}>
            <Card>
              <CardHeader title={t('organization.create.title')} />
              {createdItem && (
                <Alert className="my-8" type={AlertTypes.Success}>
                  {t('crud.create.success')}
                </Alert>
              )}
              {createItemErr && (
                <Alert className="my-8" type={AlertTypes.Error}>
                  {createItemErr}
                </Alert>
              )}
              <Formik
                initialValues={initialFormValues}
                validationSchema={Yup.object().shape({
                  commercial_contact_email: Yup.string()
                    .email('Should be a valid email address')
                    .max(255)
                    .required('Email is required'),
                  commercial_contact_name: Yup.string()
                    .max(255)
                    .required('Name is required'),
                  commercial_contact_phone: Yup.string()
                    .max(255)
                    .required('Phone is required'),
                  corporate_email: Yup.string()
                    .email('Should be a valid email address')
                    .max(255)
                    .required('Name is required'),
                  corporate_phone: Yup.string()
                    .max(255)
                    .required('Phone is required'),
                  corporate_address: Yup.string()
                    .max(255)
                    .required('Address is required'),
                })}
                onSubmit={(item, actions) => {
                  createItem({
                    item,
                    userId: userDetails?.id,
                    userRole: userDetails?.roles,
                  });
                  actions.resetForm({
                    values: initialFormValues,
                  });
                }}
              >
                {({
                  errors,
                  handleBlur,
                  handleChange,
                  handleSubmit,
                  touched,
                  values,
                }) => (
                  <form onSubmit={handleSubmit} className="mx-4">
                    <Row>
                      <Col className="mt-4" xs={12} md={6}>
                        <TextField
                          fullWidth
                          name="name"
                          margin="normal"
                          label={t('organization.create.name')}
                          value={values.name}
                          touched={touched.name}
                          error={Boolean(touched.name && errors.name)}
                          helperText={touched.name && errors.name}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          type={TextFieldType.text}
                          variant="outlined"
                        />
                      </Col>
                      <hr className="w-full my-4" />
                      <Col className="mt-4" xs={12}>
                        <Row spacing={2}>
                          <Col className="mt-4" xs={12}>
                            <h4>
                              {t('organization.create.commercialDetails')}
                            </h4>
                          </Col>
                          <Col className="mt-4" xs={12} md={6}>
                            <TextField
                              fullWidth
                              name="commercial_contact_name"
                              margin="normal"
                              label={t(
                                'organization.create.commercial_contact_name'
                              )}
                              value={values.commercial_contact_name}
                              touched={touched.commercial_contact_name}
                              error={Boolean(
                                touched.commercial_contact_name &&
                                  errors.commercial_contact_name
                              )}
                              helperText={
                                touched.commercial_contact_name &&
                                errors.commercial_contact_name
                              }
                              onBlur={handleBlur}
                              onChange={handleChange}
                              type={TextFieldType.text}
                              variant="outlined"
                            />
                          </Col>
                          <Col className="mt-4" xs={12} md={6}>
                            <TextField
                              fullWidth
                              name="commercial_contact_email"
                              margin="normal"
                              label={t(
                                'organization.create.commercial_contact_email'
                              )}
                              value={values.commercial_contact_email}
                              touched={touched.commercial_contact_email}
                              error={Boolean(
                                touched.commercial_contact_email &&
                                  errors.commercial_contact_email
                              )}
                              helperText={
                                touched.commercial_contact_email &&
                                errors.commercial_contact_email
                              }
                              onBlur={handleBlur}
                              onChange={handleChange}
                              type={TextFieldType.text}
                              variant="outlined"
                            />
                          </Col>
                          <Col className="mt-4" xs={12} md={6}>
                            <TextField
                              fullWidth
                              name="commercial_contact_phone"
                              margin="normal"
                              label={t(
                                'organization.create.commercial_contact_phone'
                              )}
                              value={values.commercial_contact_phone}
                              touched={touched.commercial_contact_phone}
                              error={Boolean(
                                touched.commercial_contact_phone &&
                                  errors.commercial_contact_phone
                              )}
                              helperText={
                                touched.commercial_contact_phone &&
                                errors.commercial_contact_phone
                              }
                              onBlur={handleBlur}
                              onChange={handleChange}
                              type={TextFieldType.text}
                              variant="outlined"
                            />
                          </Col>
                        </Row>
                      </Col>
                      <hr className="w-full my-4" />
                      <Col className="mt-4" xs={12}>
                        <Row spacing={2}>
                          <Col className="mt-4" xs={12}>
                            <h4>{t('organization.create.corporateDetails')}</h4>
                          </Col>
                          <Col className="mt-4" xs={12} md={6}>
                            <TextField
                              fullWidth
                              name="corporate_address"
                              margin="normal"
                              label={t('organization.create.corporate_address')}
                              value={values.corporate_address}
                              touched={touched.corporate_address}
                              error={Boolean(
                                touched.corporate_address &&
                                  errors.corporate_address
                              )}
                              helperText={
                                touched.corporate_address &&
                                errors.corporate_address
                              }
                              onBlur={handleBlur}
                              onChange={handleChange}
                              type={TextFieldType.text}
                              variant="outlined"
                            />
                          </Col>
                          <Col className="mt-4" xs={12} md={6}>
                            <TextField
                              fullWidth
                              name="corporate_email"
                              margin="normal"
                              label={t('organization.create.corporate_email')}
                              value={values.corporate_email}
                              touched={touched.corporate_email}
                              error={Boolean(
                                touched.corporate_email &&
                                  errors.corporate_email
                              )}
                              helperText={
                                touched.corporate_email &&
                                errors.corporate_email
                              }
                              onBlur={handleBlur}
                              onChange={handleChange}
                              type={TextFieldType.text}
                              variant="outlined"
                            />
                          </Col>
                          <Col className="mt-4" xs={12} md={6}>
                            <TextField
                              fullWidth
                              name="corporate_phone"
                              margin="normal"
                              label={t('organization.create.corporate_phone')}
                              value={values.corporate_phone}
                              touched={touched.corporate_phone}
                              error={Boolean(
                                touched.corporate_phone &&
                                  errors.corporate_phone
                              )}
                              helperText={
                                touched.corporate_phone &&
                                errors.corporate_phone
                              }
                              onBlur={handleBlur}
                              onChange={handleChange}
                              type={TextFieldType.text}
                              variant="outlined"
                            />
                          </Col>
                        </Row>
                      </Col>
                      <Divider flexItem />
                    </Row>
                    <div className="flex flex-row-reverse mt-10 mb-4">
                      <Button
                        className="mx-2"
                        color="primary"
                        size="large"
                        type="submit"
                      >
                        {t('organization.create.createBtn')}
                      </Button>
                      <Button
                        onClick={() => navigate(-1)}
                        className="mx-2"
                        color="secondary"
                        size="large"
                        type="submit"
                      >
                        {t('navigation.cancel')}
                      </Button>
                    </div>
                  </form>
                )}
              </Formik>
            </Card>
          </Col>
        </Row>
      </Container>
    </Page>
  );
};

export default CreateOrganization;
