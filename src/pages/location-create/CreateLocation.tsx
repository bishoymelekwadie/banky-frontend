import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Card, CardHeader, Container, Divider } from '@material-ui/core';
import Page from 'shared/components/Page';
import Row from 'shared/components/Row';
import Col from 'shared/components/Col';
import * as Yup from 'yup';
import { Formik } from 'formik';
import TextField, { TextFieldType } from 'shared/components/form/TextField';
import Alert, { AlertTypes } from 'shared/components/Alert';
import Button from 'shared/components/Button';
import Spinner from 'shared/components/spinner';
import { useTranslation } from 'react-i18next';
import { Location, Organization } from 'store/api';
import SelectField from 'shared/components/form/SelectField';

interface LocationListProps {
  createdItem: any;
  createItemErr: string;
  userDetails: any;
  isLoading: boolean;
  createItem: any;
  cleanStore: any;
  getOrganization: any;
  organizationList: Organization[];
}

const initialFormValues = {
  status: Location.StatusEnum.Active,
  name: '',
  address: '',
  area: '',
  city: '',
  daily_limit: 0,
  geo_location: '',
  phone: '',
  //   TODO: change this
  organization_id: undefined,
};

const LocationList = (props: LocationListProps) => {
  const {
    createdItem,
    createItem,
    createItemErr,
    userDetails,
    isLoading,
    getOrganization,
    organizationList,
  } = props;
  const { t } = useTranslation();
  const navigate = useNavigate();

  useEffect(() => {
    getOrganization({
      userId: userDetails?.id,
      userRole: userDetails?.roles,
    });
    return props.cleanStore;
  }, []);

  return (
    <Page className="mt-8" title={t('location.create.title')}>
      <Spinner loading={isLoading} />
      <Container maxWidth={false}>
        <Row>
          <Col className="mt-4" xs={12}>
            <Card>
              <CardHeader title={t('location.create.title')} />
              {createdItem && (
                <Alert className="my-8" type={AlertTypes.Success}>
                  {t('crud.create.success')}
                </Alert>
              )}
              {createItemErr && (
                <Alert className="my-8" type={AlertTypes.Error}>
                  {createItemErr}
                </Alert>
              )}
              <Formik
                initialValues={initialFormValues}
                validationSchema={Yup.object().shape({
                  name: Yup.string()
                    .max(255)
                    .required('Name is required'),
                  phone: Yup.string()
                    .max(12, 'Maximum 12 numbers')
                    .required('Phone is required'),
                  address: Yup.string()
                    .max(255)
                    .required('Address is required'),
                  area: Yup.string()
                    .max(255)
                    .required('Area is required'),
                  city: Yup.string()
                    .max(255)
                    .required('City is required'),
                  daily_limit: Yup.number().required('Daily Limit is required'),
                  geo_location: Yup.string()
                    .max(255)
                    .required('Geo Location is required'),
                })}
                onSubmit={(value, actions) => {
                  createItem({
                    userId: userDetails?.id,
                    userRole: userDetails?.roles,
                    item: value,
                  });
                  actions.resetForm({
                    values: initialFormValues,
                  });
                }}
              >
                {({
                  errors,
                  handleBlur,
                  handleChange,
                  handleSubmit,
                  isSubmitting,
                  touched,
                  values,
                }) => (
                  <form onSubmit={handleSubmit} className="mx-4">
                    <Row spacing={2}>
                      <Col className="mt-4" xs={12} md={6}>
                        <TextField
                          fullWidth
                          name="name"
                          margin="normal"
                          label={t('location.create.locationName')}
                          value={values.name}
                          touched={touched.name}
                          error={Boolean(touched.name && errors.name)}
                          helperText={touched.name && errors.name}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          type={TextFieldType.text}
                          variant="outlined"
                        />
                      </Col>
                      <Col className="mt-4" xs={12} md={6}>
                        <SelectField
                          fullWidth
                          margin="normal"
                          label="Wallet"
                          name="organization_id"
                          onChange={handleChange}
                          required
                          SelectProps={{ native: true }}
                          value={values.organization_id}
                          variant="outlined"
                        >
                          {organizationList?.map((option: any) => (
                            <option key={option.id} value={option.id}>
                              {option.name}
                            </option>
                          ))}
                        </SelectField>
                      </Col>
                      <Col className="mt-4" xs={12} md={6}>
                        <TextField
                          fullWidth
                          label={t('location.create.dailyLimit')}
                          margin="normal"
                          name="daily_limit"
                          variant="outlined"
                          value={values.daily_limit}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          type={TextFieldType.number}
                          helperText={touched.daily_limit && errors.daily_limit}
                          error={Boolean(
                            touched.daily_limit && errors.daily_limit
                          )}
                        />
                      </Col>
                      <Col className="mt-4" xs={12} md={6}>
                        <TextField
                          fullWidth
                          label={t('location.create.phone')}
                          margin="normal"
                          name="phone"
                          variant="outlined"
                          value={values.phone}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          type={TextFieldType.number}
                          helperText={touched.phone && errors.phone}
                          error={Boolean(touched.phone && errors.phone)}
                        />
                      </Col>
                      <Divider flexItem />
                      <Col className="mt-4" xs={12} md={6}>
                        <TextField
                          fullWidth
                          name="address"
                          margin="normal"
                          label="Address"
                          value={values.address}
                          touched={touched.address}
                          error={Boolean(touched.address && errors.address)}
                          helperText={touched.address && errors.address}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          type={TextFieldType.text}
                          variant="outlined"
                        />
                      </Col>
                      <Col className="mt-4" xs={12} md={6}>
                        <TextField
                          fullWidth
                          label="Area"
                          margin="normal"
                          name="area"
                          variant="outlined"
                          value={values.area}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          type={TextFieldType.text}
                          helperText={touched.area && errors.area}
                          error={Boolean(touched.area && errors.area)}
                        />
                      </Col>
                      <Col className="mt-4" xs={12} md={6}>
                        <TextField
                          fullWidth
                          label="City"
                          margin="normal"
                          name="city"
                          variant="outlined"
                          value={values.city}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          type={TextFieldType.text}
                          helperText={touched.city && errors.city}
                          error={Boolean(touched.city && errors.city)}
                        />
                      </Col>
                      <Col className="mt-4" xs={12} md={6}>
                        {/* TODO: add gio picker */}
                        <TextField
                          fullWidth
                          label={t('location.create.geoLocation')}
                          margin="normal"
                          name="geo_location"
                          variant="outlined"
                          value={values.geo_location}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          type={TextFieldType.text}
                          helperText={
                            touched.geo_location && errors.geo_location
                          }
                          error={Boolean(
                            touched.geo_location && errors.geo_location
                          )}
                        />
                      </Col>
                    </Row>
                    <div className="flex flex-row-reverse mt-10 mb-4">
                      <Button
                        className="mx-2"
                        color="primary"
                        size="large"
                        type="submit"
                      >
                        {t('location.create.createBtn')}
                      </Button>
                      <Button
                        onClick={() => navigate(-1)}
                        className="mx-2"
                        color="secondary"
                        size="large"
                        type="submit"
                      >
                        {t('navigation.cancel')}
                      </Button>
                    </div>
                  </form>
                )}
              </Formik>
            </Card>
          </Col>
        </Row>
      </Container>
    </Page>
  );
};

export default LocationList;
