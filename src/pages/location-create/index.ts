import { connect } from 'react-redux';
import { StoreReducersList } from 'shared/types';
import { createItem, cleanStore } from 'store/location/actionCreators';
import {
  getOrganization,
  GetOrganizationInputProps,
} from 'store/organization/actionCreators';
import CreateLocation from './CreateLocation';

const mapStateToProps = (state: StoreReducersList) => ({
  userDetails: state.account?.userData,
  isLoading: state.location.isCreateItemLoading,
  createdItem: state.location.createdItem,
  createItemErr: state.location.createItemErr,
  organizationList: state.organization.organizationList,
});

export default connect(mapStateToProps, (dispatch: any) => ({
  createItem: ({ userId, userRole, item }: any) =>
    dispatch(createItem({ userId, userRole, item })),
  cleanStore: () => dispatch(cleanStore()),
  getOrganization: ({
    userId,
    userRole,
    rps,
    rpi,
  }: GetOrganizationInputProps) =>
    dispatch(getOrganization({ userId, userRole, rps, rpi })),
}))(CreateLocation);
