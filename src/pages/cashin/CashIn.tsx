import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { Box, Button, Typography } from '@material-ui/core';
import Page from 'shared/components/Page';
import TextField, { TextFieldType } from 'shared/components/form/TextField';
import SelectField from 'shared/components/form/SelectField';
import { Organization, Transaction } from 'store/api';
import Alert, { AlertTypes } from 'shared/components/Alert';
import { useTranslation } from 'react-i18next';
import Spinner from 'shared/components/spinner';

const cashInSchema = Yup.object().shape({
  wallet_org_id: Yup.string().required('Wallet Organization is required'),
  wallet_phone: Yup.string().required('Wallet Number is required'),
  amount: Yup.number()
    .required('Amount is required')
    .positive()
    .integer()
    .min(10)
    .max(10000),
});

interface CashInProps {
  cashIn: any;
  cashInSuccess?: any;
  cashInErr?: any;
  getPartner: any;
  partnerList: Organization[];
  userDetails: any;
  isLoading: boolean;
}

const initialFormValues = {
  amount: 10,
  fees: 0,
  location_id: 0,
  notes: '',
  partner_org_id: 1,
  status: Transaction.StatusEnum.Pending,
  wallet_org_id: 0,
  wallet_phone: '',
  type: Transaction.TypeEnum.CashIn,
};

const CashInPage = (props: CashInProps) => {
  const {
    cashInErr,
    cashInSuccess,
    getPartner,
    userDetails,
    isLoading,
  } = props;
  const navigate = useNavigate();
  const { t } = useTranslation();

  useEffect(() => {
    getPartner({
      userId: userDetails?.id,
      userRole: userDetails?.roles,
    });
  }, []);

  const todayDate = new Date().toISOString();

  return (
    <Page title="Cash In">
      <Spinner loading={isLoading} />
      <Box display="flex" flexDirection="column" height="100%">
        <div className="bg-white p-2 lg:p-4 m-4 lg:m-8">
          {cashInSuccess && (
            <Alert className="my-8" type={AlertTypes.Success}>
              {t('transaction.cashIn.success')}
            </Alert>
          )}
          {cashInErr && (
            <Alert className="my-8" type={AlertTypes.Error}>
              {t('transaction.cashIn.failed')}
            </Alert>
          )}
          <Formik
            initialValues={initialFormValues}
            validationSchema={cashInSchema}
            onSubmit={(value, actions) => {
              props.cashIn({
                userId: userDetails?.id,
                userRole: userDetails?.roles,
                item: {
                  ...value,
                  date: todayDate,
                  created_by_id: userDetails?.id,
                  location_id: userDetails.location?.id,
                  partner_org_id: userDetails.organization.id,
                },
              });
              actions.resetForm({
                values: initialFormValues,
              });
            }}
          >
            {({
              errors,
              handleBlur,
              handleChange,
              handleSubmit,
              touched,
              values,
            }) => (
              <form onSubmit={handleSubmit}>
                <Typography color="textPrimary" variant="h2">
                  Cash In
                </Typography>
                <Typography color="textSecondary" gutterBottom variant="body2">
                  Make deposits for your customers
                </Typography>
                <div className="flex flex-row flex-wrap">
                  <div className="w-full md:w-1/2 pr-4">
                    <TextField
                      required
                      fullWidth
                      touched={touched.wallet_phone}
                      error={Boolean(
                        touched.wallet_phone && errors.wallet_phone
                      )}
                      helperText={touched.wallet_phone && errors.wallet_phone}
                      label="Wallet Number"
                      margin="normal"
                      name="wallet_phone"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      type={TextFieldType.tel}
                      value={values.wallet_phone}
                      variant="outlined"
                    />
                  </div>
                  <div className="w-full md:w-1/2 pr-4">
                    <SelectField
                      fullWidth
                      margin="normal"
                      label="Wallet"
                      name="wallet_org_id"
                      onChange={handleChange}
                      required
                      SelectProps={{ native: true }}
                      value={values.wallet_org_id}
                      variant="outlined"
                    >
                      {props.partnerList?.map((option: any) => (
                        <option key={option.id} value={option.id}>
                          {option.name}
                        </option>
                      ))}
                    </SelectField>
                  </div>
                  <div className="w-full md:w-1/2 pr-4">
                    <TextField
                      required
                      touched={touched.amount}
                      error={Boolean(touched.amount && errors.amount)}
                      fullWidth
                      helperText={touched.amount && errors.amount}
                      label="Amount"
                      margin="normal"
                      name="amount"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      type={TextFieldType.number}
                      value={values.amount}
                      variant="outlined"
                    />
                  </div>
                  <div className="w-full md:w-1/2 pr-4">
                    <TextField
                      fullWidth
                      touched={touched.notes}
                      error={Boolean(touched.notes && errors.notes)}
                      helperText={touched.notes && errors.notes}
                      label="Notes"
                      margin="normal"
                      name="notes"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      type={TextFieldType.text}
                      value={values.notes}
                      variant="outlined"
                    />
                  </div>
                </div>
                <div className="flex flex-wrap flex-row-reverse mt-5">
                  <div className="w-full md:w-1/4 pr-4 pt-4">
                    <Button
                      color="primary"
                      fullWidth
                      size="large"
                      type="submit"
                      variant="contained"
                    >
                      Confirm
                    </Button>
                  </div>
                  <div className="w-full md:w-1/4 pr-4 pt-4">
                    <Button
                      onClick={() => navigate(-1)}
                      color="secondary"
                      fullWidth
                      size="large"
                      type="submit"
                      variant="contained"
                    >
                      Cancel
                    </Button>
                  </div>
                </div>
              </form>
            )}
          </Formik>
        </div>
      </Box>
    </Page>
  );
};

export default CashInPage;
