import { connect } from 'react-redux';
import { StoreReducersList } from 'shared/types';
import { getPartner, GetPartnerInputProps } from 'store/partner/actionCreators';
import { cashIn } from 'store/wallet';
import CashInPage from './CashIn';

const mapStateToProps = (state: StoreReducersList) => ({
  userDetails: state.account?.userData,
  walletTypes: state.wallet.availableTypes,
  partnerList: state.partner.partnerList,
  isLoading: state.wallet.isLoadingCashIn,
  cashInErr: state.wallet.cashInErrorMessage,
  cashInSuccess: state.wallet.cashIn,
});

const mapDispatchToProps = (dispatch: any) => ({
  cashIn: (details: any) => dispatch(cashIn(details)),
  getPartner: ({ userId, userRole, rps, rpi }: GetPartnerInputProps) =>
    dispatch(getPartner({ userId, userRole, rps, rpi })),
});

export default connect(mapStateToProps, mapDispatchToProps)(CashInPage);
