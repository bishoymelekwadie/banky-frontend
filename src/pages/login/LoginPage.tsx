import React, { useEffect } from 'react';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { Box, Container, Link, Typography } from '@material-ui/core';
import Page from 'shared/components/Page';
import Button from 'shared/components/Button';
import TextField, { TextFieldType } from 'shared/components/form/TextField';
import Alert, { AlertTypes } from 'shared/components/Alert';

interface LoginPageProps {
  readonly login: any;
  readonly userDetails: any;
  readonly loginErr: string;
}

const LoginPage = (props: LoginPageProps) => {
  const { userDetails, loginErr } = props;
  // const { t, i18n } = useTranslation();
  const navigate = useNavigate();

  useEffect(() => {
    if (userDetails) navigate('/app/dashboard');
  }, [navigate, userDetails]);

  return (
    <Page className="lg:mt-0 flex items-center" title="Login">
      <Container
        maxWidth="sm"
        className="flex items-center justify-center border-solid border-2 border-primary p-5"
      >
        <img
          className="mx-auto block w-1/3"
          src="/static/logo.svg"
          alt="company logo"
        />
        {loginErr && (
          <Alert className="my-8" type={AlertTypes.Error}>
            {loginErr}
          </Alert>
        )}
        <div className="mb-4 mt-6">
          <Typography color="textPrimary" variant="h2">
            Sign in
          </Typography>
        </div>
        <Formik
          initialValues={{
            email: '',
            password: '',
          }}
          validationSchema={Yup.object().shape({
            email: Yup.string()
              .email('Must be a valid email')
              .max(255)
              .required('Email is required'),
            password: Yup.string()
              .max(255)
              .required('Password is required'),
          })}
          onSubmit={value => props.login(value)}
        >
          {({
            errors,
            handleBlur,
            handleChange,
            handleSubmit,
            isSubmitting,
            touched,
            values,
          }) => (
            <form onSubmit={handleSubmit}>
              <TextField
                fullWidth
                name="email"
                margin="normal"
                label="Email Address"
                value={values.email}
                touched={touched.email}
                error={Boolean(touched.email && errors.email)}
                helperText={touched.email && errors.email}
                onBlur={handleBlur}
                onChange={handleChange}
                type={TextFieldType.email}
                variant="outlined"
              />
              <TextField
                fullWidth
                label="Password"
                margin="normal"
                name="password"
                variant="outlined"
                value={values.password}
                onBlur={handleBlur}
                onChange={handleChange}
                type={TextFieldType.password}
                helperText={touched.password && errors.password}
                error={Boolean(touched.password && errors.password)}
              />
              <Box my={2}>
                <Button
                  color="primary"
                  disabled={isSubmitting}
                  fullWidth
                  size="large"
                  type="submit"
                >
                  Sign in now
                </Button>
              </Box>
              <Typography color="textSecondary" variant="body1">
                Forget your password?{' '}
                <Link component={RouterLink} to="/register" variant="h6">
                  Reset password
                </Link>
              </Typography>
            </form>
          )}
        </Formik>
      </Container>
    </Page>
  );
};

export default LoginPage;
