import { connect } from 'react-redux';
import { CredentialsProps, StoreReducersList } from 'shared/types';
import { login } from 'store/account/actionCreators';
import LoginPage from './LoginPage';

const mapStateToProps = (state: StoreReducersList) => ({
  walletTypes: state.wallet.availableTypes,
  userDetails: state.account?.userData,
  isLoginLoading: state.account?.isLoginLoading,
  loginErr: state.account?.loginErrorMessage,
});

export default connect(mapStateToProps, (dispatch: any) => ({
  login: (credentials: CredentialsProps) => dispatch(login(credentials)),
}))(LoginPage);
