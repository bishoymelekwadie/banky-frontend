import React from 'react';
import { Navigate } from 'react-router-dom';
import DashboardLayout from 'shared/layouts/DashboardLayout';
import MainLayout from 'shared/layouts/MainLayout';
import AccountView from 'pages/account/AccountView';
import CashInView from 'pages/cashin';
import CashOutView from 'pages/cashout';
import LocationListView from 'pages/location-list';
import LocationCreateView from 'pages/location-create';

import OrganizationListView from 'pages/organization-list';
import OrganizationCreateView from 'pages/organization-create';
import PartnerListView from 'pages/partner-list';
import PartnerCreateView from 'pages/partner-create';
import TransactionListView from 'pages/transaction-list';
import UserListView from 'pages/user-list';
import UserCreateView from 'pages/user-create';

import UpperManagementDashboardView from 'pages/reports/DashboardView';
import DashboardView from 'pages/simple-dashboard';
import LoginView from 'pages/login';
import NotFoundView from 'pages/errors/NotFoundView';
import RegisterView from 'pages/auth/RegisterView';
import SettingsView from 'pages/settings/SettingsView';
import OverView from 'pages/OverView';

const routes = [
  {
    path: 'app',
    element: <DashboardLayout />,
    children: [
      { path: 'account', element: <AccountView /> },
      { path: 'transaction', element: <TransactionListView /> },
      { path: 'user', element: <UserListView /> },
      { path: 'user/create', element: <UserCreateView /> },
      { path: 'location', element: <LocationListView /> },
      { path: 'location/create', element: <LocationCreateView /> },
      { path: 'organization', element: <OrganizationListView /> },
      { path: 'organization/create', element: <OrganizationCreateView /> },
      { path: 'partner', element: <PartnerListView /> },
      { path: 'partner/create', element: <PartnerCreateView /> },
      { path: 'cash-in', element: <CashInView /> },
      { path: 'cash-out', element: <CashOutView /> },
      { path: 'admin-dashboard', element: <UpperManagementDashboardView /> },
      { path: 'dashboard', element: <DashboardView /> },
      { path: 'settings', element: <SettingsView /> },
      { path: '*', element: <Navigate to="/404" /> },
    ],
  },
  {
    path: '/',
    element: <MainLayout />,
    children: [
      { path: 'login', element: <LoginView /> },
      { path: 'register', element: <RegisterView /> },
      { path: 'overview', element: <OverView /> },
      { path: '404', element: <NotFoundView /> },
      { path: '/', element: <Navigate to="/login" /> },
      { path: '*', element: <Navigate to="/404" /> },
    ],
  },
];

export default routes;
