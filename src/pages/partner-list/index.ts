import { connect } from 'react-redux';
import { StoreReducersList } from 'shared/types';
import ListPartner from './ListPartner';

const mapStateToProps = (state: StoreReducersList) => ({
  isLoading: state.partner.isGetListLoading,
});

export default connect(mapStateToProps)(ListPartner);
