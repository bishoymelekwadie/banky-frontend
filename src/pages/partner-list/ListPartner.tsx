import React from 'react';
import { Container } from '@material-ui/core';
import Page from 'shared/components/Page';
import Row from 'shared/components/Row';
import Col from 'shared/components/Col';
import PartnerTable from 'shared/components/partner-table';
import Spinner from 'shared/components/spinner';

interface ListPartnerProps {
  isLoading: boolean;
}

const ListPartner = (props: ListPartnerProps) => {
  const { isLoading } = props;

  return (
    <Page className="mt-8" title="Partner List">
      <Spinner loading={isLoading} />
      <Container maxWidth={false}>
        <Row>
          <Col className="mt-4" xs={12}>
            <PartnerTable />
          </Col>
        </Row>
      </Container>
    </Page>
  );
};

export default ListPartner;
