import { connect } from 'react-redux';
import { StoreReducersList } from 'shared/types';
import ListTransaction from './ListTransaction';

const mapStateToProps = (state: StoreReducersList) => ({
  isLoading: state.transaction.isGetListLoading,
});

export default connect(mapStateToProps)(ListTransaction);
