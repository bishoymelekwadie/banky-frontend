import React from 'react';
import { Container } from '@material-ui/core';
import Page from 'shared/components/Page';
import Row from 'shared/components/Row';
import Col from 'shared/components/Col';
import TransactionTable from 'shared/components/transaction-table';
import Spinner from 'shared/components/spinner';

interface ListTransactionProps {
  isLoading: boolean;
}

const ListTransaction = (props: ListTransactionProps) => {
  const { isLoading } = props;

  return (
    <Page className="mt-8" title="Transaction List">
      <Spinner loading={isLoading} />
      <Container maxWidth={false}>
        <Row>
          <Col className="mt-4" xs={12}>
            <TransactionTable />
          </Col>
        </Row>
      </Container>
    </Page>
  );
};

export default ListTransaction;
