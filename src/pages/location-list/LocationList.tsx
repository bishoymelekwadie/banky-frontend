import React from 'react';
import { Container } from '@material-ui/core';
import Page from 'shared/components/Page';
import Row from 'shared/components/Row';
import Col from 'shared/components/Col';
import LocationsTable from 'shared/components/locations-table';
import Spinner from 'shared/components/spinner';

interface LocationListProps {
  isLoading: boolean;
}

const LocationList = (props: LocationListProps) => {
  const { isLoading } = props;
  return (
    <Page className="mt-8" title="Location List">
      <Spinner loading={isLoading} />
      <Container maxWidth={false}>
        <Row>
          <Col className="mt-4" xs={12}>
            <LocationsTable />
          </Col>
        </Row>
      </Container>
    </Page>
  );
};

export default LocationList;
