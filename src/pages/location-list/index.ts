import { connect } from 'react-redux';
import { StoreReducersList } from 'shared/types';
import LocationList from './LocationList';

const mapStateToProps = (state: StoreReducersList) => ({
  isLoading: state.location.isGetListLoading,
});

export default connect(mapStateToProps)(LocationList);
