import { connect } from 'react-redux';
import { StoreReducersList } from 'shared/types';
import ListUser from './ListUser';

const mapStateToProps = (state: StoreReducersList) => ({
  isLoading: state.user.isGetListLoading,
});

export default connect(mapStateToProps)(ListUser);
