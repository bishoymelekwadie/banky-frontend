import React from 'react';
import { Container } from '@material-ui/core';
import Page from 'shared/components/Page';
import Row from 'shared/components/Row';
import Col from 'shared/components/Col';
import UserTable from 'shared/components/user-table';
import Spinner from 'shared/components/spinner';

interface ListUserProps {
  isLoading: boolean;
}

const ListUser = (props: ListUserProps) => {
  const { isLoading } = props;

  return (
    <Page className="mt-8" title="User List">
      <Spinner loading={isLoading} />
      <Container maxWidth={false}>
        <Row>
          <Col className="mt-4" xs={12}>
            <UserTable />
          </Col>
        </Row>
      </Container>
    </Page>
  );
};

export default ListUser;
