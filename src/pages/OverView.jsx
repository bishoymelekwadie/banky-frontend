import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { Container, Grid, Link } from '@material-ui/core';
import ReactCardFlip from 'react-card-flip';
import theme from 'config/styling/theme';

class Card extends React.Component {
  constructor() {
    super();
    this.state = {
      isFlipped: false,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    e.preventDefault();
    this.setState(prevState => ({ isFlipped: !prevState.isFlipped }));
  }

  render() {
    const { title, description, background, linkTo = '' } = this.props;
    return (
      <Grid item sm={6} xs={12} onClick={this.handleClick} style={{}}>
        <ReactCardFlip
          containerStyle={{
            alignContent: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            background,
            display: 'flex',
            height: 200,
            borderRadius: 5,
            color: 'white',
            margin: 10,
            padding: 40,
          }}
          isFlipped={this.state.isFlipped}
          flipDirection="horizontal"
        >
          <div>
            <h1>{title}</h1>
            <Link
              style={{
                position: 'absolute',
                bottom: 0,
                right: 0,
                background: 'white',
                padding: 10,
              }}
              component={RouterLink}
              to={linkTo}
              variant="h5"
            >
              Start Now
            </Link>
          </div>
          <div>
            <h4>{description}</h4>
          </div>
        </ReactCardFlip>
      </Grid>
    );
  }
}
function Overview(props) {
  return (
    <Container maxWidth={false} style={{ marginTop: 30 }}>
      <Grid container spacing={1}>
        <Card
          linkTo="/app/cash-in"
          background={theme.palette.primary.main}
          title="Cash In"
          description="You can cash-in for your customers"
        />
        <Card
          linkTo="/app/cash-out"
          background={theme.palette.success.main}
          title="Cash Out"
          description="You can cash-out for your customers"
        />
        <Card
          linkTo=""
          background={theme.palette.warning.main}
          title="Collection"
          description="You can collect money"
        />
        <Card
          linkTo=""
          background={theme.palette.primary.main}
          title="Bill"
          description="You can pay bills for your customers"
        />
      </Grid>
    </Container>
  );
}

export default Overview;
