import React from 'react';
import { Container } from '@material-ui/core';
import Page from 'shared/components/Page';
import Row from 'shared/components/Row';
import Col from 'shared/components/Col';
import TransactionsTable from 'shared/components/transaction-table';
import InfoCard from './InfoCard';

interface SimpleDashboardProps {
  readonly withdrawAmount: number;
  readonly depositAmount: number;
  readonly settleInAmount: number;
}

const Dashboard = (props: SimpleDashboardProps) => {
  const {
    withdrawAmount = 25000,
    depositAmount = 24500,
    settleInAmount = 21000,
  } = props;

  return (
    <Page className="mt-8" title="Dashboard">
      <Container maxWidth={false}>
        <Row>
          <Col md={4} sm={6} xs={12} className="sm:pr-4 pb-4">
            <InfoCard amount={withdrawAmount} title="Cash-In Amount" />
          </Col>
          <Col md={4} sm={6} xs={12} className="sm:pr-4 pb-4">
            <InfoCard amount={depositAmount} title="Cash-Out Amount" />
          </Col>
          <Col md={4} sm={6} xs={12} className="sm:pr-4 md:pr-0 pb-4">
            <InfoCard amount={settleInAmount} title="To Settle-In Amount" />
          </Col>
          <Col className="mt-4" xs={12}>
            <TransactionsTable toggleViewAllBtn />
          </Col>
        </Row>
      </Container>
    </Page>
  );
};

export default Dashboard;
