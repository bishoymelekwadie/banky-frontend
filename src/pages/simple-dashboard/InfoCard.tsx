import React from 'react';
import { Avatar, Box, Card, CardContent, Typography } from '@material-ui/core';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import MoneyIcon from '@material-ui/icons/Money';

const InfoCard = (props: any) => {
  const { className, title, amount } = props;

  return (
    <Card className={`${className}`}>
      <CardContent>
        <div className="flex flex-row justify-between">
          <div>
            <Typography color="textSecondary" gutterBottom variant="h6">
              {title}
            </Typography>
            <Typography color="textPrimary" variant="h3">
              {amount}
            </Typography>
          </div>
          <div>
            <Avatar className="">
              <MoneyIcon />
            </Avatar>
          </div>
        </div>
        <Box mt={2} display="flex" alignItems="center">
          <ArrowDownwardIcon className="" />
          <Typography className="" variant="body2">
            12%
          </Typography>
          <Typography color="textSecondary" variant="caption">
            Since last month
          </Typography>
        </Box>
      </CardContent>
    </Card>
  );
};

export default InfoCard;
