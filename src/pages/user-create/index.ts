import { connect } from 'react-redux';
import { StoreReducersList } from 'shared/types';
import {
  GetLocationInputProps,
  getLocations,
} from 'store/location/actionCreators';
import { createItem, cleanStore } from 'store/user/actionCreators';
import CreateUser from './CreateUser';

const mapStateToProps = (state: StoreReducersList) => ({
  userDetails: state.account.userData,
  isLoading: state.user.isCreateItemLoading,
  createdItem: state.user.createdItem,
  createItemErr: state.user.createItemErr,
  locationsList: state.location.locationsList,
});

export default connect(mapStateToProps, (dispatch: any) => ({
  createItem: ({ userId, userRole, item }: any) =>
    dispatch(createItem({ userId, userRole, item })),
  cleanStore: () => dispatch(cleanStore()),
  getLocations: ({ userId, userRole, rps, rpi }: GetLocationInputProps) =>
    dispatch(getLocations({ userId, userRole, rps, rpi })),
}))(CreateUser);
