import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Card, CardHeader, Container, Divider } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import * as Yup from 'yup';
import { Formik } from 'formik';
import Page from 'shared/components/Page';
import Row from 'shared/components/Row';
import Col from 'shared/components/Col';
import TextField, { TextFieldType } from 'shared/components/form/TextField';
import Alert, { AlertTypes } from 'shared/components/Alert';
import Button from 'shared/components/Button';
import Spinner from 'shared/components/spinner';
import SelectField from 'shared/components/form/SelectField';
import { User } from 'store/api';

interface CreateUserProps {
  createdItem: any;
  createItemErr: string;
  userDetails: any;
  isLoading: boolean;
  createItem: any;
  cleanStore: any;
  locationsList: any;
  getLocations: any;
}

interface CreateUserInput {
  email: string;
  full_name: string;
  location_id: number;
  mobile_number: string;
  password: string;
  photoUrl: string;
  status: User.StatusEnum;
  //   userType: User.;
  user_name: string;
}

const initialFormValues = {
  email: '',
  password: '',
  full_name: '',
  user_name: '',
  mobile_number: '',
  location_id: 0,
  photoUrl: '',
  status: User.StatusEnum.Active,
};

const CreateUser = (props: CreateUserProps) => {
  const {
    createdItem,
    createItem,
    createItemErr,
    userDetails,
    isLoading,
    locationsList,
    getLocations,
  } = props;
  const { t } = useTranslation();
  const navigate = useNavigate();

  useEffect(() => {
    getLocations({
      userId: userDetails?.id,
      userRole: userDetails?.roles,
    });

    return props.cleanStore;
  }, []);

  return (
    <Page className="mt-8" title={t('user.create.title')}>
      <Spinner loading={isLoading} />
      <Container maxWidth={false}>
        <Row>
          <Col className="mt-4" xs={12}>
            <Card>
              <CardHeader title={t('user.create.title')} />
              {createdItem && (
                <Alert className="my-8" type={AlertTypes.Success}>
                  {t('crud.create.success')}
                </Alert>
              )}
              {createItemErr && (
                <Alert className="my-8" type={AlertTypes.Error}>
                  {createItemErr}
                </Alert>
              )}
              <Formik
                initialValues={initialFormValues}
                validationSchema={Yup.object().shape({
                  email: Yup.string()
                    .email('it has to be a valid email address')
                    .max(255)
                    .required('Email is required'),
                  password: Yup.string()
                    .max(255)
                    .required('Password is required'),
                  full_name: Yup.string()
                    .max(255)
                    .required('Full Name is required'),
                  user_name: Yup.string()
                    .max(255)
                    .required('User Name is required'),
                  mobile_number: Yup.string()
                    .max(255)
                    .required('Mobile Number is required'),
                })}
                onSubmit={(item: CreateUserInput, actions) => {
                  createItem({
                    item: {
                      organization_id: userDetails.organization.id,
                      ...item,
                    },
                    userId: userDetails?.id,
                    userRole: userDetails?.roles,
                  });
                  actions.resetForm({
                    values: initialFormValues,
                  });
                }}
              >
                {({
                  errors,
                  handleBlur,
                  handleChange,
                  handleSubmit,
                  isSubmitting,
                  touched,
                  values,
                }) => (
                  <form onSubmit={handleSubmit} className="mx-4">
                    <div className="flex flex-row flex-wrap">
                      <div className="w-full md:w-1/2 pr-4">
                        <TextField
                          required
                          fullWidth
                          name="name"
                          margin="normal"
                          label={t('user.create.email')}
                          value={values.email}
                          touched={touched.email}
                          error={Boolean(touched.email && errors.email)}
                          helperText={touched.email && errors.email}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          type={TextFieldType.text}
                          variant="outlined"
                        />
                      </div>
                      <div className="w-full md:w-1/2 pr-4">
                        <TextField
                          fullWidth
                          name="name"
                          margin="normal"
                          label={t('user.create.fullName')}
                          value={values.full_name}
                          touched={touched.full_name}
                          error={Boolean(touched.full_name && errors.full_name)}
                          helperText={touched.full_name && errors.full_name}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          type={TextFieldType.text}
                          variant="outlined"
                        />
                      </div>
                      <div className="w-full md:w-1/2 pr-4">
                        <TextField
                          fullWidth
                          name="name"
                          margin="normal"
                          label={t('user.create.mobileNumber')}
                          value={values.mobile_number}
                          touched={touched.mobile_number}
                          error={Boolean(
                            touched.mobile_number && errors.mobile_number
                          )}
                          helperText={
                            touched.mobile_number && errors.mobile_number
                          }
                          onBlur={handleBlur}
                          onChange={handleChange}
                          type={TextFieldType.text}
                          variant="outlined"
                        />
                      </div>
                      <div className="w-full md:w-1/2 pr-4">
                        <TextField
                          fullWidth
                          name="name"
                          margin="normal"
                          label={t('user.create.password')}
                          value={values.password}
                          touched={touched.password}
                          error={Boolean(touched.password && errors.password)}
                          helperText={touched.password && errors.password}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          type={TextFieldType.text}
                          variant="outlined"
                        />
                      </div>
                      <div className="w-full md:w-1/2 pr-4">
                        <SelectField
                          fullWidth
                          margin="normal"
                          label="Wallet"
                          name="location_id"
                          onChange={handleChange}
                          required
                          SelectProps={{ native: true }}
                          value={values.location_id}
                          variant="outlined"
                        >
                          {locationsList?.map((option: any) => (
                            <option key={option.id} value={option.id}>
                              {option.name}
                            </option>
                          ))}
                        </SelectField>
                      </div>
                      <div className="w-full md:w-1/2 pr-4">
                        <TextField
                          fullWidth
                          name="name"
                          margin="normal"
                          label={t('user.create.user_name')}
                          value={values.user_name}
                          touched={touched.user_name}
                          error={Boolean(touched.user_name && errors.user_name)}
                          helperText={touched.user_name && errors.user_name}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          type={TextFieldType.text}
                          variant="outlined"
                        />
                      </div>
                      <Divider flexItem />
                    </div>
                    <div className="flex flex-row-reverse mt-10 mb-4">
                      <Button
                        className="mx-2"
                        color="primary"
                        size="large"
                        type="submit"
                      >
                        {t('user.create.createBtn')}
                      </Button>
                      <Button
                        onClick={() => navigate(-1)}
                        className="mx-2"
                        color="secondary"
                        size="large"
                        type="submit"
                      >
                        {t('navigation.cancel')}
                      </Button>
                    </div>
                  </form>
                )}
              </Formik>
            </Card>
          </Col>
        </Row>
      </Container>
    </Page>
  );
};

export default CreateUser;
