import { connect } from 'react-redux';
import { StoreReducersList } from 'shared/types';
import { cashOut } from 'store/wallet';
import { getPartner, GetPartnerInputProps } from 'store/partner/actionCreators';
import CashOutPage from './CashOut';

const mapStateToProps = (state: StoreReducersList) => ({
  userDetails: state.account?.userData,
  walletTypes: state.wallet.availableTypes,
  partnerList: state.partner.partnerList,
  isLoading: state.wallet.isLoadingCashOut,
  cashOutErr: state.wallet.cashOutErrorMessage,
  cashOutSuccess: state.wallet.cashOut,
});

const mapDispatchToProps = (dispatch: any) => ({
  cashOut: (details: any) => dispatch(cashOut(details)),
  getPartner: ({ userId, userRole, rps, rpi }: GetPartnerInputProps) =>
    dispatch(getPartner({ userId, userRole, rps, rpi })),
});

export default connect(mapStateToProps, mapDispatchToProps)(CashOutPage);
