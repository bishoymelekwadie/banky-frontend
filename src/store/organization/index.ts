/* eslint-disable no-param-reassign */
import produce from 'immer';
import { organizationActions } from './actionCreators';

/**
 * Initial State for the Data resource (Service Providers)
 */
const organizationInitialState = {
  isGetListLoading: false,
  organizationList: [],
  getOrganizationErrorMessage: undefined,
};

const organizationReducer = (state = organizationInitialState, action: any) =>
  produce(state, (draft: any) => {
    switch (action.type) {
      /**
       * ***********************************
       * get-organization handlers
       * ***********************************
       */
      case organizationActions.GET_LIST_START_LOADING:
        draft.getOrganizationErrorMessage = undefined;
        draft.isGetListLoading = true;
        break;
      case organizationActions.GET_LIST_SUCCEED:
        draft.organizationList = action?.list;
        draft.pageSize = action?.pageSize;
        draft.pageIndex = action?.pageIndex;
        draft.totalCount = action?.totalCount;
        draft.isGetListLoading = false;
        break;
      case organizationActions.GET_LIST_FAILED:
        draft.getOrganizationErrorMessage = action?.errMessage;
        draft.isGetListLoading = false;
        break;

      /**
       * ***********************************
       * create-item handlers
       * ***********************************
       */
      case organizationActions.CREATE_ITEM_START_LOADING:
        draft.createItemErrorMessage = undefined;
        draft.isCreateItemLoading = true;
        break;
      case organizationActions.CREATE_ITEM_SUCCEED:
        draft.createdItem = action?.item;
        draft.isCreateItemLoading = false;
        break;
      case organizationActions.CREATE_ITEM_FAILED:
        draft.createItemErrorMessage = action?.errMessage;
        draft.isCreateItemLoading = false;
        break;
      case organizationActions.CLEAN_STORE:
        draft = organizationInitialState;
        break;
      default:
        break;
    }
    return draft;
  });

export default organizationReducer;
