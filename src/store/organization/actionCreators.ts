/* eslint-disable no-nested-ternary */
/* eslint-disable no-return-await */
import { PaginationSuccessProps } from 'shared/types';
import { Organization, OrganizationApi } from 'store/api';

const organizationApi = new OrganizationApi({ basePath: process.env.API_URL });

export const organizationActions = {
  CLEAN_STORE: 'ORGANIZATION/CLEAN_STORE',
  GET_LIST: 'ORGANIZATION/GET_LIST',
  GET_LIST_START_LOADING: 'ORGANIZATION/GET_LIST_START_LOADING',
  GET_LIST_FAILED: 'ORGANIZATION/GET_LIST_FAILED',
  GET_LIST_SUCCEED: 'ORGANIZATION/GET_LIST_SUCCEED',
  CREATE_ITEM: 'ORGANIZATION/CREATE_ITEM',
  CREATE_ITEM_START_LOADING: 'ORGANIZATION/CREATE_ITEM_START_LOADING',
  CREATE_ITEM_FAILED: 'ORGANIZATION/CREATE_ITEM_FAILED',
  CREATE_ITEM_SUCCEED: 'ORGANIZATION/CREATE_ITEM_SUCCEED',
};

/**
 * clean store
 */
export const cleanStore = () => ({
  type: organizationActions.CLEAN_STORE,
});

/**
 * Get Transactions List
 */
export const startLoadingGetList = () => ({
  type: organizationActions.GET_LIST_START_LOADING,
});

export const getOrganizationSucceed = ({
  list,
  pageIndex,
  pageSize,
  totalCount,
}: PaginationSuccessProps) => ({
  type: organizationActions.GET_LIST_SUCCEED,
  list,
  pageIndex,
  pageSize,
  totalCount,
});

export const getOrganizationFailed = (errMessage: any, meta?: any) => ({
  type: organizationActions.GET_LIST_FAILED,
  errMessage,
  meta,
});

export interface GetOrganizationInputProps {
  userId: number;
  userRole?: string;
  rps?: number;
  rpi?: number;
}

export function getOrganization({
  userId,
  userRole = '2',
  rps,
  rpi,
}: GetOrganizationInputProps) {
  return async (dispatch: any) => {
    try {
      dispatch(startLoadingGetList());
      const res = await organizationApi.getAllOrganizations(
        23,
        undefined,
        rps,
        rpi,
        Organization.TypeEnum.CioPartener.toString(),
        { headers: { accept: 'application/json' } }
      );

      const { results, pageIndex, pageSize, totalCount } = res;

      dispatch(
        getOrganizationSucceed({
          list: results,
          pageIndex,
          pageSize,
          totalCount,
        })
      );
    } catch (error) {
      dispatch(
        getOrganizationFailed(
          'Something went wrong,please check your credentials'
        )
      );
    }
  };
}

/**
 * Get Transactions List
 */
export const startLoadingCreateItem = () => ({
  type: organizationActions.CREATE_ITEM_START_LOADING,
});

export const createItemSucceed = ({ item, message }: any) => ({
  type: organizationActions.CREATE_ITEM_SUCCEED,
  item,
  message,
});

export const createItemFailed = (errMessage: any, meta?: any) => ({
  type: organizationActions.CREATE_ITEM_FAILED,
  errMessage,
  meta,
});

export interface CreateItemInputProps {
  userId: number;
  userRole?: string;
  item?: any;
}

export function createItem({
  userId = 22,
  userRole = '2',
  item: inputItem,
}: CreateItemInputProps) {
  return async (dispatch: any) => {
    try {
      dispatch(startLoadingCreateItem());
      const data: any = await organizationApi.createOrganization(
        userId,
        inputItem,
        userRole,
        {
          headers: { accept: 'application/json' },
        }
      );

      if (data)
        dispatch(
          createItemSucceed({
            item: data,
            message: 'Item Created',
          })
        );
      else dispatch(createItemFailed('Something went wrong'));
    } catch (error) {
      dispatch(createItemFailed('Something went wrong'));
    }
  };
}
