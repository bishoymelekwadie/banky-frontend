/* eslint-disable no-nested-ternary */
/* eslint-disable no-return-await */
import { PaginationSuccessProps } from 'shared/types';
import { PartnerApi, Organization } from 'store/api';

const partnerApi = new PartnerApi({ basePath: process.env.API_URL });

export const partnerActions = {
  CLEAN_STORE: 'PARTNER/CLEAN_STORE',
  GET_LIST: 'PARTNER/GET_LIST',
  GET_LIST_START_LOADING: 'PARTNER/GET_LIST_START_LOADING',
  GET_LIST_FAILED: 'PARTNER/GET_LIST_FAILED',
  GET_LIST_SUCCEED: 'PARTNER/GET_LIST_SUCCEED',
  CREATE_ITEM: 'PARTNER/CREATE_ITEM',
  CREATE_ITEM_START_LOADING: 'PARTNER/CREATE_ITEM_START_LOADING',
  CREATE_ITEM_FAILED: 'PARTNER/CREATE_ITEM_FAILED',
  CREATE_ITEM_SUCCEED: 'PARTNER/CREATE_ITEM_SUCCEED',
};

/**
 * clean store
 */
export const cleanStore = () => ({
  type: partnerActions.CLEAN_STORE,
});

/**
 * Get Transactions List
 */
export const startLoadingGetList = () => ({
  type: partnerActions.GET_LIST_START_LOADING,
});

export const getPartnerSucceed = ({
  list,
  pageIndex,
  pageSize,
  totalCount,
}: PaginationSuccessProps) => ({
  type: partnerActions.GET_LIST_SUCCEED,
  list,
  pageIndex,
  pageSize,
  totalCount,
});

export const getPartnerFailed = (errMessage: any, meta?: any) => ({
  type: partnerActions.GET_LIST_FAILED,
  errMessage,
  meta,
});

export interface GetPartnerInputProps {
  userId: number;
  userRole?: string;
  rps?: number;
  rpi?: number;
}

export function getPartner({
  userId,
  userRole = '2',
  rps,
  rpi,
}: GetPartnerInputProps) {
  return async (dispatch: any) => {
    try {
      dispatch(startLoadingGetList());
      const res = await partnerApi.getAllOrganizations(
        userId,
        userRole,
        rps,
        rpi,
        Organization.TypeEnum.WalletPartener.toString(),
        { headers: { accept: 'application/json' } }
      );

      const { results, pageIndex, pageSize, totalCount } = res;

      dispatch(
        getPartnerSucceed({
          list: results,
          pageIndex,
          pageSize,
          totalCount,
        })
      );
    } catch (error) {
      dispatch(
        getPartnerFailed('Something went wrong,please check your credentials')
      );
    }
  };
}

/**
 * Get Transactions List
 */
export const startLoadingCreateItem = () => ({
  type: partnerActions.CREATE_ITEM_START_LOADING,
});

export const createItemSucceed = ({ item, message }: any) => ({
  type: partnerActions.CREATE_ITEM_SUCCEED,
  item,
  message,
});

export const createItemFailed = (errMessage: any, meta?: any) => ({
  type: partnerActions.CREATE_ITEM_FAILED,
  errMessage,
  meta,
});

export interface CreateItemInputProps {
  userId: number;
  userRole?: string;
  item?: any;
}

export function createItem({
  userId,
  userRole = '2',
  item: inputItem,
}: CreateItemInputProps) {
  return async (dispatch: any) => {
    try {
      dispatch(startLoadingCreateItem());
      const data: any = await partnerApi.createOrganization(
        userId,
        inputItem,
        userRole,
        {
          headers: { accept: 'application/json' },
        }
      );
      if (data)
        dispatch(
          createItemSucceed({
            item: data,
            message: 'Item Created',
          })
        );
      else dispatch(createItemFailed('Something went wrong'));
    } catch (error) {
      dispatch(
        createItemFailed('Something went wrong,please check your credentials')
      );
    }
  };
}
