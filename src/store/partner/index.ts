/* eslint-disable no-param-reassign */
import produce from 'immer';
import { partnerActions } from './actionCreators';

/**
 * Initial State for the Data resource (Service Providers)
 */
const partnerInitialState = {
  isGetListLoading: false,
  partnerList: [],
  getPartnerErrorMessage: undefined,
};

const partnerReducer = (state = partnerInitialState, action: any) =>
  produce(state, (draft: any) => {
    switch (action.type) {
      /**
       * ***********************************
       * get-partner handlers
       * ***********************************
       */
      case partnerActions.GET_LIST_START_LOADING:
        draft.getPartnerErrorMessage = undefined;
        draft.isGetListLoading = true;
        break;
      case partnerActions.GET_LIST_SUCCEED:
        draft.partnerList = action?.list;
        draft.pageSize = action?.pageSize;
        draft.pageIndex = action?.pageIndex;
        draft.totalCount = action?.totalCount;
        draft.isGetListLoading = false;
        break;
      case partnerActions.GET_LIST_FAILED:
        draft.getPartnerErrorMessage = action?.errMessage;
        draft.isGetListLoading = false;
        break;

      /**
       * ***********************************
       * create-item handlers
       * ***********************************
       */
      case partnerActions.CREATE_ITEM_START_LOADING:
        draft.createItemErrorMessage = undefined;
        draft.isCreateItemLoading = true;
        break;
      case partnerActions.CREATE_ITEM_SUCCEED:
        draft.createdItem = action?.item;
        draft.isCreateItemLoading = false;
        break;
      case partnerActions.CREATE_ITEM_FAILED:
        draft.createItemErrorMessage = action?.errMessage;
        draft.isCreateItemLoading = false;
        break;
      case partnerActions.CLEAN_STORE:
        draft = partnerInitialState;
        break;
      default:
        break;
    }
    return draft;
  });

export default partnerReducer;
