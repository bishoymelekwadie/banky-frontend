/* eslint-disable no-nested-ternary */
/* eslint-disable no-return-await */
import { PaginationSuccessProps } from 'shared/types';
import { TransactionApi } from 'store/api';

const transactionApi = new TransactionApi({ basePath: process.env.API_URL });

export const transactionActions = {
  GET_LIST: 'Transaction/GET_LIST',
  GET_LIST_START_LOADING: 'Transaction/GET_LIST_START_LOADING',
  GET_LIST_FAILED: 'Transaction/GET_LIST_FAILED',
  GET_LIST_SUCCEED: 'Transaction/GET_LIST_SUCCEED',
  CREATE_ITEM: 'Transaction/CREATE_ITEM',
  CREATE_ITEM_START_LOADING: 'Transaction/CREATE_ITEM_START_LOADING',
  CREATE_ITEM_FAILED: 'Transaction/CREATE_ITEM_FAILED',
  CREATE_ITEM_SUCCEED: 'Transaction/CREATE_ITEM_SUCCEED',
};

/**
 * Get Transactions List
 */
export const startLoadingGetList = () => ({
  type: transactionActions.GET_LIST_START_LOADING,
});

export const getTransactionSucceed = ({
  list,
  pageIndex,
  pageSize,
  totalCount,
}: PaginationSuccessProps) => ({
  type: transactionActions.GET_LIST_SUCCEED,
  list,
  pageIndex,
  pageSize,
  totalCount,
});

export const getTransactionFailed = (errMessage: any, meta?: any) => ({
  type: transactionActions.GET_LIST_FAILED,
  errMessage,
  meta,
});

export interface GetTransactionInputProps {
  userId: number;
  userRole?: string;
  rps?: number;
  rpi?: number;
}

export function getTransaction({
  userId = 23,
  userRole = '2',
  rps,
  rpi,
  orgId,
  locationId,
  fromDate,
  toDate,
}: GetTransactionInputProps & {
  orgId?: number;
  locationId?: number;
  fromDate?: string;
  toDate?: string;
}) {
  return async (dispatch: any) => {
    try {
      dispatch(startLoadingGetList());
      const res = await transactionApi.getAllTransactions(
        userId,
        userRole,
        rps,
        rpi,
        orgId,
        locationId,
        userId,
        fromDate,
        toDate,
        { headers: { accept: 'application/json' } }
      );

      const { results, pageIndex, pageSize, totalCount } = res;

      dispatch(
        getTransactionSucceed({
          list: results,
          pageIndex,
          pageSize,
          totalCount,
        })
      );
    } catch (error) {
      dispatch(
        getTransactionFailed(
          'Something went wrong,please check your credentials'
        )
      );
    }
  };
}

/**
 * Get Transactions List
 */
export const startLoadingCreateItem = () => ({
  type: transactionActions.CREATE_ITEM_START_LOADING,
});

export const createItemSucceed = ({ item, message }: any) => ({
  type: transactionActions.CREATE_ITEM_SUCCEED,
  item,
  message,
});

export const createItemFailed = (errMessage: any, meta?: any) => ({
  type: transactionActions.CREATE_ITEM_FAILED,
  errMessage,
  meta,
});

export interface CreateItemInputProps {
  userId: number;
  userRole?: string;
  item?: any;
}

export function createItem({
  userId,
  userRole = '2',
  item: inputItem,
}: CreateItemInputProps) {
  return async (dispatch: any) => {
    try {
      dispatch(startLoadingCreateItem());
      const res: any = await transactionApi.createTransaction(
        userId,
        inputItem,
        undefined,
        {
          headers: { accept: 'application/json' },
        }
      );

      const { item, message } = res;

      dispatch(
        createItemSucceed({
          item,
          message,
        })
      );
    } catch (error) {
      dispatch(
        createItemFailed('Something went wrong,please check your credentials')
      );
    }
  };
}
