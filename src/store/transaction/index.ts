/* eslint-disable no-param-reassign */
import produce from 'immer';
import { transactionActions } from './actionCreators';

/**
 * Initial State for the Data resource (Service Providers)
 */
const userInitialState = {
  isGetListLoading: false,
  transactionList: [],
  getTransactionErrorMessage: undefined,
};

const transactionReducer = (state = userInitialState, action: any) =>
  produce(state, (draft: any) => {
    switch (action.type) {
      /**
       * ***********************************
       * get-transaction handlers
       * ***********************************
       */
      case transactionActions.GET_LIST_START_LOADING:
        draft.getTransactionErrorMessage = undefined;
        draft.isGetListLoading = true;
        break;
      case transactionActions.GET_LIST_SUCCEED:
        draft.transactionList = action?.list;
        draft.pageSize = action?.pageSize;
        draft.pageIndex = action?.pageIndex;
        draft.totalCount = action?.totalCount;
        draft.isGetListLoading = false;
        break;
      case transactionActions.GET_LIST_FAILED:
        draft.getTransactionErrorMessage = action?.errMessage;
        draft.isGetListLoading = false;
        break;

      /**
       * ***********************************
       * create-item handlers
       * ***********************************
       */
      case transactionActions.CREATE_ITEM_START_LOADING:
        draft.createItemErrorMessage = undefined;
        draft.isCreateItemLoading = true;
        break;
      case transactionActions.CREATE_ITEM_SUCCEED:
        draft.createdItem = action?.item;
        draft.isCreateItemLoading = false;
        break;
      case transactionActions.CREATE_ITEM_FAILED:
        draft.createItemErrorMessage = action?.errMessage;
        draft.isCreateItemLoading = false;
        break;

      default:
        break;
    }
    return draft;
  });

export default transactionReducer;
