/* eslint-disable no-param-reassign */
import produce from 'immer';
import { userActions } from './actionCreators';

/**
 * Initial State for the Data resource (Service Providers)
 */
const userInitialState = {
  isGetListLoading: false,
  userList: [],
  getUserErrorMessage: undefined,
};

const userReducer = (state = userInitialState, action: any) =>
  produce(state, (draft: any) => {
    switch (action.type) {
      /**
       * ***********************************
       * get-user handlers
       * ***********************************
       */
      case userActions.GET_LIST_START_LOADING:
        draft.getUserErrorMessage = undefined;
        draft.isGetListLoading = true;
        break;
      case userActions.GET_LIST_SUCCEED:
        draft.userList = action?.list;
        draft.pageSize = action?.pageSize;
        draft.pageIndex = action?.pageIndex;
        draft.totalCount = action?.totalCount;
        draft.isGetListLoading = false;
        break;
      case userActions.GET_LIST_FAILED:
        draft.getUserErrorMessage = action?.errMessage;
        draft.isGetListLoading = false;
        break;

      /**
       * ***********************************
       * create-item handlers
       * ***********************************
       */
      case userActions.CREATE_ITEM_START_LOADING:
        draft.createItemErrorMessage = undefined;
        draft.isCreateItemLoading = true;
        break;
      case userActions.CREATE_ITEM_SUCCEED:
        draft.createdItem = action?.item;
        draft.isCreateItemLoading = false;
        break;
      case userActions.CREATE_ITEM_FAILED:
        draft.createItemErrorMessage = action?.errMessage;
        draft.isCreateItemLoading = false;
        break;
      case userActions.CLEAN_STORE:
        draft = userInitialState;
        break;

      default:
        break;
    }
    return draft;
  });

export default userReducer;
