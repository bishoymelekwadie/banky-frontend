/* eslint-disable no-nested-ternary */
/* eslint-disable no-return-await */
import { PaginationSuccessProps } from 'shared/types';
import { UserApi } from 'store/api';

const userApi = new UserApi({ basePath: process.env.API_URL });

export const userActions = {
  CLEAN_STORE: 'ORGANIZATION/CLEAN_STORE',
  GET_LIST: 'User/GET_LIST',
  GET_LIST_START_LOADING: 'User/GET_LIST_START_LOADING',
  GET_LIST_FAILED: 'User/GET_LIST_FAILED',
  GET_LIST_SUCCEED: 'User/GET_LIST_SUCCEED',
  CREATE_ITEM: 'User/CREATE_ITEM',
  CREATE_ITEM_START_LOADING: 'User/CREATE_ITEM_START_LOADING',
  CREATE_ITEM_FAILED: 'User/CREATE_ITEM_FAILED',
  CREATE_ITEM_SUCCEED: 'User/CREATE_ITEM_SUCCEED',
};

/**
 * clean store
 */
export const cleanStore = () => ({
  type: userActions.CLEAN_STORE,
});

/**
 * Get Transactions List
 */
export const startLoadingGetList = () => ({
  type: userActions.GET_LIST_START_LOADING,
});

export const getUserSucceed = ({
  list,
  pageIndex,
  pageSize,
  totalCount,
}: PaginationSuccessProps) => ({
  type: userActions.GET_LIST_SUCCEED,
  list,
  pageIndex,
  pageSize,
  totalCount,
});

export const getUserFailed = (errMessage: any, meta?: any) => ({
  type: userActions.GET_LIST_FAILED,
  errMessage,
  meta,
});

export interface GetUserInputProps {
  userId: number;
  userRole?: string;
  rps?: number;
  rpi?: number;
}

export function getUser({
  userId,
  userRole = '2',
  rps,
  rpi,
  orgId,
  locationId,
}: GetUserInputProps & {
  orgId?: number;
  locationId?: number;
}) {
  return async (dispatch: any) => {
    try {
      dispatch(startLoadingGetList());
      const res = await userApi.getAllUsers(
        userId,
        userRole,
        rps,
        rpi,
        orgId,
        locationId,
        { headers: { accept: 'application/json' } }
      );

      const { results, pageIndex, pageSize, totalCount } = res;

      dispatch(
        getUserSucceed({
          list: results,
          pageIndex,
          pageSize,
          totalCount,
        })
      );
    } catch (error) {
      dispatch(
        getUserFailed('Something went wrong,please check your credentials')
      );
    }
  };
}

/**
 * Get Transactions List
 */
export const startLoadingCreateItem = () => ({
  type: userActions.CREATE_ITEM_START_LOADING,
});

export const createItemSucceed = ({ item, message }: any) => ({
  type: userActions.CREATE_ITEM_SUCCEED,
  item,
  message,
});

export const createItemFailed = (errMessage: any, meta?: any) => ({
  type: userActions.CREATE_ITEM_FAILED,
  errMessage,
  meta,
});

export interface CreateItemInputProps {
  userId: number;
  userRole?: string;
  item?: any;
}

export function createItem({
  userId,
  userRole = '2',
  item: inputItem,
}: CreateItemInputProps) {
  return async (dispatch: any) => {
    try {
      dispatch(startLoadingCreateItem());
      const data: any = await userApi.addUser(userId, inputItem, userRole, {
        headers: { accept: 'application/json' },
      });

      if (data)
        dispatch(
          createItemSucceed({
            item: data,
            message: 'Item Created',
          })
        );
      else dispatch(createItemFailed('Something went wrong'));
    } catch (error) {
      dispatch(
        createItemFailed('Something went wrong,please check your credentials')
      );
    }
  };
}
