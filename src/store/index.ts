import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import { StoreReducersList } from 'shared/types';

/**
 ***** reducers ******
 */
import LocationReducer from './location';
import AccountReducer from './account';
import OrganizationReducer from './organization';
import WalletReducer from './wallet';
import TransactionsReducer from './transaction';
import UsersReducer from './user';
import PartnerReducer from './partner';

const reducersList: StoreReducersList = {
  account: AccountReducer,
  wallet: WalletReducer,
  location: LocationReducer,
  transaction: TransactionsReducer,
  user: UsersReducer,
  organization: OrganizationReducer,
  partner: PartnerReducer,
};

const combinedReducers = combineReducers(reducersList);

const middlewares = [thunk];

if (process.env.NODE_ENV === 'development') {
  // @ts-ignore
  middlewares.push(logger);
}

// @ts-ignore
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const store = createStore(
  combinedReducers,
  composeEnhancers(applyMiddleware(...middlewares))
);

export default store;
