/* eslint-disable no-param-reassign */
import produce from 'immer';
import { locationActions } from './actionCreators';

/**
 * Initial State for the Data resource (Service Providers)
 */
const locationInitialState = {
  isGetListLoading: false,
  locationsList: [],
  getLocationsErrorMessage: undefined,
};

const location = (state = locationInitialState, action: any) =>
  produce(state, (draft: any) => {
    switch (action.type) {
      /**
       * ***********************************
       * get-locations handlers
       * ***********************************
       */
      case locationActions.GET_LIST_START_LOADING:
        draft.getLocationsErrorMessage = undefined;
        draft.isGetListLoading = true;
        break;
      case locationActions.GET_LIST_SUCCEED:
        draft.locationsList = action?.list;
        draft.pageSize = action?.pageSize;
        draft.pageIndex = action?.pageIndex;
        draft.totalCount = action?.totalCount;
        draft.isGetListLoading = false;
        break;
      case locationActions.GET_LIST_FAILED:
        draft.getLocationsErrorMessage = action?.errMessage;
        draft.isGetListLoading = false;
        break;

      /**
       * ***********************************
       * create-item handlers
       * ***********************************
       */
      case locationActions.CREATE_ITEM_START_LOADING:
        draft.createItemErrorMessage = undefined;
        draft.isCreateItemLoading = true;
        break;
      case locationActions.CREATE_ITEM_SUCCEED:
        draft.createdItem = action?.item;
        draft.isCreateItemLoading = false;
        break;
      case locationActions.CREATE_ITEM_FAILED:
        draft.createItemErrorMessage = action?.errMessage;
        draft.isCreateItemLoading = false;
        break;
      case locationActions.CLEAN_STORE:
        draft = locationInitialState;
        break;

      default:
        break;
    }
    return draft;
  });

export default location;
