/* eslint-disable no-nested-ternary */
/* eslint-disable no-return-await */
import { PaginationSuccessProps } from 'shared/types';
import { LocationApi } from 'store/api';

const locationApi = new LocationApi({ basePath: process.env.API_URL });

export const locationActions = {
  CLEAN_STORE: 'LOCATIONS/CLEAN_STORE',
  GET_LIST: 'LOCATIONS/GET_LIST',
  GET_LIST_START_LOADING: 'LOCATIONS/GET_LIST_START_LOADING',
  GET_LIST_FAILED: 'LOCATIONS/GET_LIST_FAILED',
  GET_LIST_SUCCEED: 'LOCATIONS/GET_LIST_SUCCEED',
  CREATE_ITEM: 'LOCATIONS/CREATE_ITEM',
  CREATE_ITEM_START_LOADING: 'LOCATIONS/CREATE_ITEM_START_LOADING',
  CREATE_ITEM_FAILED: 'LOCATIONS/CREATE_ITEM_FAILED',
  CREATE_ITEM_SUCCEED: 'LOCATIONS/CREATE_ITEM_SUCCEED',
};

/**
 * clean store
 */
export const cleanStore = () => ({
  type: locationActions.CLEAN_STORE,
});

/**
 * Get Transactions List
 */
export const startLoadingGetList = () => ({
  type: locationActions.GET_LIST_START_LOADING,
});

export const getLocationsSucceed = ({
  list,
  pageIndex,
  pageSize,
  totalCount,
}: PaginationSuccessProps) => ({
  type: locationActions.GET_LIST_SUCCEED,
  list,
  pageIndex,
  pageSize,
  totalCount,
});

export const getLocationsFailed = (errMessage: any, meta?: any) => ({
  type: locationActions.GET_LIST_FAILED,
  errMessage,
  meta,
});

export interface GetLocationInputProps {
  userId: number;
  userRole?: string;
  rps?: number;
  rpi?: number;
}

export function getLocations({
  userId,
  userRole = '2',
  rps,
  rpi,
}: GetLocationInputProps) {
  return async (dispatch: any) => {
    try {
      dispatch(startLoadingGetList());
      const res = await locationApi.getAllLocations(
        23,
        undefined,
        rps,
        rpi,
        undefined,
        { headers: { accept: 'application/json' } }
      );

      const { results, pageIndex, pageSize, totalCount } = res;

      dispatch(
        getLocationsSucceed({
          list: results,
          pageIndex,
          pageSize,
          totalCount,
        })
      );
    } catch (error) {
      dispatch(
        getLocationsFailed('Something went wrong,please check your credentials')
      );
    }
  };
}

/**
 * Get Transactions List
 */
export const startLoadingCreateItem = () => ({
  type: locationActions.CREATE_ITEM_START_LOADING,
});

export const createItemSucceed = ({ item, message }: any) => ({
  type: locationActions.CREATE_ITEM_SUCCEED,
  item,
  message,
});

export const createItemFailed = (errMessage: any, meta?: any) => ({
  type: locationActions.CREATE_ITEM_FAILED,
  errMessage,
  meta,
});

export interface CreateItemInputProps {
  userId: number;
  userRole?: string;
  item?: any;
}

export function createItem({
  userId,
  userRole = '2',
  item: inputItem,
}: CreateItemInputProps) {
  return async (dispatch: any) => {
    try {
      dispatch(startLoadingCreateItem());
      const data: any = await locationApi.createLocation(
        userId,
        inputItem,
        userRole,
        {
          headers: { accept: 'application/json' },
        }
      );

      if (data)
        dispatch(
          createItemSucceed({
            item: data,
            message: 'Item Created',
          })
        );
      else dispatch(createItemFailed('Something went wrong'));
    } catch (error) {
      dispatch(
        createItemFailed('Something went wrong,please check your credentials')
      );
    }
  };
}
