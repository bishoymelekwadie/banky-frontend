import { TransactionApi } from 'store/api';
import { CreateItemInputProps } from './organization/actionCreators';

const transactionApi = new TransactionApi({ basePath: process.env.API_URL });

const CASH_IN_START = 'WALLET/CASH_IN_START';
const CASH_IN_SUCCESS = 'WALLET/CASH_IN_SUCCESS';
const CASH_IN_ERROR = 'WALLET/CASH_IN_ERROR';
const CASH_OUT_START = 'WALLET/CASH_OUT_START';
const CASH_OUT_SUCCESS = 'WALLET/CASH_OUT_SUCCESS';
const CASH_OUT_ERROR = 'WALLET/CASH_OUT_ERROR';

export const cashInStarted = () => ({
  type: CASH_IN_START,
});

export const cashInSuccess = ({ item }: any) => ({
  type: CASH_IN_SUCCESS,
  item,
});

export const cashInError = (errorMessage: any) => ({
  type: CASH_IN_ERROR,
  payload: errorMessage,
});

export const cashIn = ({
  userId,
  userRole = '2',
  item: inputItem,
}: CreateItemInputProps) => async (dispatch: any) => {
  dispatch(cashInStarted());
  try {
    dispatch(cashInStarted());
    const data = await transactionApi.createTransaction(
      userId,
      inputItem,
      '23',
      {
        headers: { accept: 'application/json' },
      }
    );
    if (data) dispatch(cashInSuccess({ item: data }));
    else dispatch(cashInError('error'));
  } catch (exc) {
    dispatch(cashInError('error'));
  }
};

export const cashOutStarted = () => ({
  type: CASH_OUT_START,
});

export const cashOutSuccess = ({ item }: any) => ({
  type: CASH_OUT_SUCCESS,
  item,
});

export const cashOutError = (errorMessage: any) => ({
  type: CASH_OUT_ERROR,
  payload: errorMessage,
});

export const cashOut = ({
  userId,
  userRole = '2',
  item: inputItem,
}: CreateItemInputProps) => async (dispatch: any) => {
  dispatch(cashOutStarted());
  try {
    dispatch(cashOutStarted());
    const data = await transactionApi.createTransaction(
      userId,
      inputItem,
      '23',
      {
        headers: { accept: 'application/json' },
      }
    );
    if (data) dispatch(cashOutSuccess({ item: data }));
    else dispatch(cashOutError('error'));
  } catch (exc) {
    dispatch(cashOutError('error'));
  }
};

const INITIAL_STATE: any = {
  isLoading: false,
  errorMessage: undefined,
};

const walletReducer = (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case CASH_IN_START:
      return {
        ...state,
        isLoadingCashIn: true,
      };
    case CASH_IN_SUCCESS:
      return {
        ...state,
        isLoadingCashIn: false,
        cashIn: action.item,
      };
    case CASH_IN_ERROR:
      return {
        ...state,
        isLoadingCashIn: false,
        cashInErrorMessage: action.payload,
      };
    case CASH_OUT_START:
      return {
        ...state,
        isLoadingCashOut: true,
      };
    case CASH_OUT_SUCCESS:
      return {
        ...state,
        isLoadingCashOut: false,
        cashOut: action.item,
      };
    case CASH_OUT_ERROR:
      return {
        ...state,
        isLoadingCashOut: false,
        cashOutErrorMessage: action.payload,
      };

    default:
      return state;
  }
};

export default walletReducer;
