/* eslint-disable no-param-reassign */
import produce from 'immer';
import { authActions } from './actionCreators';

/**
 * Initial State for the Data resource (Service Providers)
 */
const authInitialState = {
  isLoginLoading: false,
  loginErrorMessage: undefined,
  isCheckEmailIfExistLoading: false,
  userData: {
    addedOn: '2021-04-10T21:53:47.404Z',
    created_by: 'string',
    email: 'bishoymelekwadie@gmail.com',
    full_name: 'Bishoy Melek',
    id: 5,
    lastLoginDate: '2021-04-10T21:53:47.404Z',
    location: {
      address: 'string',
      area: 'string',
      city: 'string',
      daily_limit: 0,
      geo_location: 'string',
      id: 1,
      name: 'string',
      organization_id: 0,
      phone: 'string',
      status: 'active',
    },
    location_id: 1,
    mobile_number: 'string',
    organization: {
      commercial_contact_email: 'string',
      commercial_contact_name: 'string',
      commercial_contact_phone: 'string',
      corporate_address: 'string',
      corporate_email: 'string',
      corporate_phone: 'string',
      id: 1,
      name: 'string',
      status: 'active',
      type: 'coroperate',
    },
    organization_id: 1,
    password: 'string',
    photoUrl: 'string',
    roles: [
      {
        id: 0,
        name: 'string',
        permissions: 'string',
      },
    ],
    status: 'active',
    userType: 'agent',
    user_name: 'bishoy',
  },
};

const auth = (state = authInitialState, action: any) =>
  produce(state, (draft: any) => {
    switch (action.type) {
      /**
       * ***********************************
       * Logout handlers
       * ***********************************
       */
      case authActions.LOGOUT:
        draft.userData = undefined;
        draft.profile = undefined;
        draft.message = undefined;
        draft.isLoginLoading = false;
        break;

      /**
       * ***********************************
       * Login handlers
       * ***********************************
       */
      case authActions.LOGIN_START_LOADING:
        draft.loginErrorMessage = undefined;
        draft.isLoginLoading = true;
        break;
      case authActions.LOGIN_SUCCEED:
        draft.userData = action?.userData;
        draft.message = action?.message;
        draft.isLoginLoading = false;
        break;
      case authActions.LOGIN_FAILED:
        draft.loginErrorMessage = action?.errMessage;
        draft.isLoginLoading = false;
        break;

      default:
        break;
    }
    return draft;
  });

export default auth;
