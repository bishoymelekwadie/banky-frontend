/* eslint-disable no-nested-ternary */
/* eslint-disable no-return-await */
// import generateTraceParent from 'utils/store';
// import { v4 as uuidv4 } from 'uuid';
import { CredentialsProps } from 'shared/types';

export const authActions = {
  LOGIN: 'AUTH/LOGIN',
  LOGOUT: 'AUTH/LOGOUT',
  LOGIN_START_LOADING: 'AUTH/LOGIN_START_LOADING',
  LOGIN_FAILED: 'AUTH/LOGIN_FAILED',
  LOGIN_SUCCEED: 'AUTH/LOGIN_SUCCEED',
};

/** Logout */
export function logout() {
  return async (dispatch: any) => {
    await localStorage.removeItem('userToken');
    await localStorage.removeItem('userType');
    return dispatch({
      type: authActions.LOGOUT,
    });
  };
}

/**
 * Login user to the portal
 */
export const startLoadingLogin = () => ({
  type: authActions.LOGIN_START_LOADING,
});

export const loginSucceed = (userData: any, message?: string) => ({
  type: authActions.LOGIN_SUCCEED,
  message,
  userData,
});

export const loginFailed = (errMessage: any, meta?: any) => ({
  type: authActions.LOGIN_FAILED,
  errMessage,
  meta,
});

export function login(credentials: CredentialsProps) {
  return async (dispatch: any) => {
    try {
      dispatch(startLoadingLogin());
      //   const { email, password } = credentials;
      //   const res = await instance({
      //     url: `/${userName}/${password}`,
      //     data: {
      //       userName,
      //       password,
      //     },
      //     method: 'GET',
      //     headers: {
      //       SystemId: 'uis-web-ui',
      //       traceparent: generateTraceParent(),
      //     },
      //   });
      //   const { status = 200, data = { detail: true } } = res;

      // TODO:remove this
      const data = { detail: true };
      const status = 200;

      if (status === 200) {
        dispatch(loginSucceed(data, 'Welcome back'));
      } else if (status === 400) {
        dispatch(loginFailed(data));
      } else {
        dispatch(loginFailed('Something went wrong'));
      }
    } catch (error) {
      dispatch(
        loginFailed('Something went wrong,please check your credentials')
      );
    }
  };
}
