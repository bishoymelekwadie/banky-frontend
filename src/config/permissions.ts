export enum UserRoles {
  Agent = 'Agent',
  LocationManager = 'LocationManager',
  Partner = 'Partner',
  OperationManager = 'OperationManager',
  FinancialOfficer = 'FinancialOfficer',
}

export const allPermissions = {
  viewRegistrationRequests: {
    id: 'viewRegistrationRequests',
    description: '',
  },
  viewBookingsList: {
    id: 'viewBookingsList',
    description: '',
  },
  viewClaimsList: {
    id: 'viewClaimsList',
    description: '',
  },
  createBooking: {
    id: 'createBooking',
    description: '',
  },
};

export const agentPermissions = [
  allPermissions.viewRegistrationRequests,
  allPermissions.viewBookingsList,
];

export const partnerPermissions = [
  allPermissions.viewBookingsList,
  allPermissions.createBooking,
];

export const locationManagerPermissions = [
  allPermissions.viewBookingsList,
  allPermissions.createBooking,
];

export const financialOfficerPermissions = [
  allPermissions.viewBookingsList,
  allPermissions.createBooking,
];

export const operationManagerPermissions = [
  allPermissions.viewBookingsList,
  allPermissions.createBooking,
];

export const permissions = {
  [UserRoles.Agent]: agentPermissions,
  [UserRoles.Partner]: partnerPermissions,
  [UserRoles.LocationManager]: locationManagerPermissions,
  [UserRoles.OperationManager]: operationManagerPermissions,
  [UserRoles.FinancialOfficer]: financialOfficerPermissions,
};

export const authorize = (role: UserRoles, permission: any) => {
  const hasAuthorization = permissions[role].find(
    onePermission => onePermission.id === permission.id
  );
  return hasAuthorization;
};
