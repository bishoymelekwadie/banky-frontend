export const TRANSLATION_EN = {
  navigation: {
    back: 'Back',
    cancel: 'Cancel',
  },
  project: {
    title: 'Project Name',
  },
  user: {
    list: 'Users List',
    create: {
      title: 'Create New User',
      phone: 'Phone',
      email: 'Email',
      fullName: 'Full Name',
      password: 'Password',
      user_name: 'Username',
      mobileNumber: 'Mobile number',
      createBtn: 'Create',
    },
  },
  location: {
    list: 'Locations List',
    create: {
      title: 'Create New Location',
      locationName: 'Location Name',
      dailyLimit: 'Daily Limit',
      geoLocation: 'Geo Location',
      phone: 'Phone',
      createBtn: 'Create',
    },
  },
  organization: {
    list: 'Organization List',
    create: {
      title: 'Create Organization',
      commercialDetails: 'Commercial Contact Details',
      corporateDetails: 'Corporate Details',
      createBtn: 'Create',
      name: 'Org Name',
      commercial_contact_name: 'Commercial contact Name',
      commercial_contact_email: 'Commercial contact Email',
      commercial_contact_phone: 'Commercial contact Phone',
      corporate_address: 'Corporate Address',
      corporate_email: 'Corporate Email',
      corporate_phone: 'Corporate Phone',
    },
  },
  partner: {
    list: 'Partner List',
    create: {
      title: 'Create Partner',
      commercialDetails: 'Commercial Contact Details',
      corporateDetails: 'Corporate Details',
      createBtn: 'Create',
      name: 'Org Name',
      commercial_contact_name: 'Commercial contact Name',
      commercial_contact_email: 'Commercial contact Email',
      commercial_contact_phone: 'Commercial contact Phone',
      corporate_address: 'Corporate Address',
      corporate_email: 'Corporate Email',
      corporate_phone: 'Corporate Phone',
    },
  },
  transaction: {
    list: 'Transaction List',
    cashIn: {
      success: 'Transaction confirmed successfully',
      failed: 'Sorry,Transaction Failed',
    },
    cashOut: {
      success: 'Transaction confirmed successfully',
      failed: 'Sorry,Transaction Failed',
    },
  },
  crud: {
    create: {
      success: 'Item created successfully',
    },
  },
};
