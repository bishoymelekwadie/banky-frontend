/* eslint-disable @typescript-eslint/indent */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Card, CardHeader } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { DataGrid } from '@material-ui/data-grid';
import { Transaction } from 'store/api';

interface TransactionListProps {
  transactionList?: Transaction[];
  getTransaction?: any;
  userDetails: any;
  toggleViewAllBtn?: boolean;
}

const columns = [
  { field: 'id', headerName: 'ID', width: 70, sortable: false },
  { field: 'amount', headerName: 'Amount', width: 120, sortable: false },
  {
    field: 'wallet_phone',
    headerName: 'Phone',
    width: 130,
    sortable: false,
  },
  {
    field: 'type',
    headerName: 'Type',
    width: 130,
    sortable: false,
  },
  {
    field: 'status',
    headerName: 'Status',
    width: 130,
    sortable: false,
    renderCell: (params: any) => {
      const bgColorAndColor =
        params.row.status === Transaction.StatusEnum.Success
          ? 'bg-success'
          : params.row.status === Transaction.StatusEnum.Failed
          ? 'bg-error'
          : 'bg-warning text-black';
      return (
        <div className="w-full">
          <div
            style={{ height: 30, borderRadius: 20 }}
            className={`w-2/3 px-1 flex items-center justify-center text-white ${bgColorAndColor}`}
          >
            {params.row.status}
          </div>
        </div>
      );
    },
  },
  {
    field: 'date',
    headerName: 'Date',
    width: 170,
    sortable: false,
    renderCell: (params: any) => (
      <div className="w-full">{new Date(params.row.date).toLocaleString()}</div>
    ),
  },
  {
    field: 'notes',
    headerName: 'Notes',
    width: 240,
    sortable: false,
  },
];

const TransactionListTable = (props: TransactionListProps) => {
  const {
    transactionList = [],
    userDetails,
    getTransaction,
    toggleViewAllBtn = false,
  } = props;

  const { t } = useTranslation();
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(10);

  useEffect(() => {
    getTransaction({
      userId: userDetails?.id,
      userRole: userDetails?.roles,
      rpi: page + 1,
      rps: pageSize,
    });
  }, [page, pageSize]);

  return (
    <Card>
      <CardHeader title={t('transaction.list')} />
      <PerfectScrollbar>
        <div style={{ height: 500, width: '100%' }}>
          <DataGrid
            rows={transactionList}
            columns={columns}
            checkboxSelection={false}
            rowsPerPageOptions={[20, 50]}
            page={page}
            onPageChange={(params: any) => setPage(params.page)}
            onPageSizeChange={(params: any) => setPageSize(params.pageSize)}
            pageSize={pageSize}
          />
        </div>
      </PerfectScrollbar>
    </Card>
  );
};

export default TransactionListTable;
