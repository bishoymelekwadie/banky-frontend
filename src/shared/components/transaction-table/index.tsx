import { connect } from 'react-redux';
import { StoreReducersList } from 'shared/types';
import {
  GetTransactionInputProps,
  getTransaction,
} from 'store/transaction/actionCreators';
import TransactionTable from './TransactionTable';

const mapStateToProps = (state: StoreReducersList) => ({
  userDetails: state.account?.userData,
  isGetTransactionLoading: state.user.isGetTransactionLoading,
  transactionList: state.transaction.transactionList,
  getTransactionErr: state.user.getTransactionErrorMessage,
});

const mapDispatchToProps = (dispatch: any) => ({
  getTransaction: ({ userId, userRole, rps, rpi }: GetTransactionInputProps) =>
    dispatch(getTransaction({ userId, userRole, rps, rpi })),
});

export default connect(mapStateToProps, mapDispatchToProps)(TransactionTable);
