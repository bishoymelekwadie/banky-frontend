import { connect } from 'react-redux';
import { StoreReducersList } from 'shared/types';
import { GetUserInputProps, getUser } from 'store/user/actionCreators';
import UserTable from './UserTable';

const mapStateToProps = (state: StoreReducersList) => ({
  userDetails: state.account?.userData,
  isGetUserLoading: state.user.isgetUserLoading,
  userList: state.user.userList,
  getUserErr: state.user.getUserErrorMessage,
});

const mapDispatchToProps = (dispatch: any) => ({
  getUser: ({ userId, userRole, rps, rpi }: GetUserInputProps) =>
    dispatch(getUser({ userId, userRole, rps, rpi })),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserTable);
