import React from 'react';
import MuiAlert from '@material-ui/lab/Alert';

export enum AlertTypes {
  Error = 'error',
  Warning = 'warning',
  Info = 'info',
  Success = 'success',
}

export interface AlertProps {
  children: any;
  type: AlertTypes;
  className?: string;
}

export default function Alert(props: AlertProps) {
  return (
    <div className={`${props.className}`}>
      <MuiAlert severity={props.type}>{props.children}</MuiAlert>
    </div>
  );
}
