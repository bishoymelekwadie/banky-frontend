import { connect } from 'react-redux';
import { StoreReducersList } from 'shared/types';
import {
  GetPartnerInputProps,
  getPartner,
} from 'store/partner/actionCreators';
import PartnerTable from './PartnerTable';

const mapStateToProps = (state: StoreReducersList) => ({
  userDetails: state.account?.userData,
  isGetPartnerLoading: state.user.isgetPartnerLoading,
  partnerList: state.partner.partnerList,
  getPartnerErr: state.user.getPartnerErrorMessage,
});

const mapDispatchToProps = (dispatch: any) => ({
  getPartner: ({ userId, userRole, rps, rpi }: GetPartnerInputProps) =>
    dispatch(getPartner({ userId, userRole, rps, rpi })),
});

export default connect(mapStateToProps, mapDispatchToProps)(PartnerTable);
