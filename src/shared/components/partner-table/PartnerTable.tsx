import React, { useEffect, useState } from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Card, CardHeader } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import { DataGrid } from '@material-ui/data-grid';
import IconButton from '@material-ui/core/IconButton';
import CreateIcon from '@material-ui/icons/Create';

interface PartnerListProps {
  partnerList?: any;
  getPartner?: any;
  userDetails: any;
}

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'name', headerName: 'Name', width: 120 },
  { field: 'status', headerName: 'Status', width: 100 },
  { field: 'commercial_contact_name', headerName: 'Contact Name', width: 150 },
  {
    field: 'commercial_contact_phone',
    headerName: 'Contact phone',
    width: 160,
  },
  {
    field: 'commercial_contact_email',
    headerName: 'Contact email',
    width: 160,
  },
  { field: 'corporate_address', headerName: 'Company Address', width: 175 },
  { field: 'corporate_phone', headerName: 'Company Phone', width: 170 },
];

const PartnerListTable = (props: PartnerListProps) => {
  const { partnerList = [], userDetails, getPartner } = props;
  const navigate = useNavigate();
  const { t } = useTranslation();
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(10);

  useEffect(() => {
    getPartner({
      userId: userDetails?.id,
      userRole: userDetails?.roles,
      rpi: page + 1,
      rps: pageSize,
    });
  }, [page, pageSize]);

  return (
    <Card>
      <CardHeader
        title={t('partner.list')}
        action={
          <IconButton
            onClick={() => navigate('/app/partner/create')}
            aria-label="create"
          >
            <CreateIcon />
          </IconButton>
        }
      />
      <PerfectScrollbar>
        <div style={{ height: 700, width: '100%' }}>
          <DataGrid
            rows={partnerList}
            columns={columns}
            checkboxSelection={false}
            rowsPerPageOptions={[20, 50]}
            page={page}
            onPageChange={(params: any) => setPage(params.page)}
            onPageSizeChange={(params: any) => setPageSize(params.pageSize)}
            pageSize={pageSize}
          />
        </div>
      </PerfectScrollbar>
    </Card>
  );
};

export default PartnerListTable;
