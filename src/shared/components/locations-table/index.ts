import { connect } from 'react-redux';
import { StoreReducersList } from 'shared/types';
import {
  GetLocationInputProps,
  getLocations,
} from 'store/location/actionCreators';
import LocationsTable from './LocationsTable';

const mapStateToProps = (state: StoreReducersList) => ({
  userDetails: state.account?.userData,
  isGetLocationsLoading: state.user.isgetLocationsLoading,
  locationsList: state.location.locationsList,
  getLocationsErr: state.user.getLocationsErrorMessage,
});

const mapDispatchToProps = (dispatch: any) => ({
  getLocations: ({ userId, userRole, rps, rpi }: GetLocationInputProps) =>
    dispatch(getLocations({ userId, userRole, rps, rpi })),
});

export default connect(mapStateToProps, mapDispatchToProps)(LocationsTable);
