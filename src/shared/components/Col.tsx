import React from 'react';
import { Grid } from '@material-ui/core';

interface ColProps {
  children: any;
  spacing?: any;
  xs?: any;
  md?: any;
  lg?: any;
  sm?: any;
  className?: string;
}

const Col = (props: ColProps) => {
  const { className = '', children, spacing, xs, md, lg, sm } = props;
  return (
    <Grid
      item
      spacing={spacing}
      xs={xs}
      md={md}
      lg={lg}
      sm={sm}
      className={`${className}`}
    >
      {children}
    </Grid>
  );
};

export default Col;
