/* eslint-disable no-console */
/* eslint-disable react/button-has-type */
/* eslint-disable @typescript-eslint/indent */
/* eslint-disable no-nested-ternary */
import React from 'react';

interface ButtonProps {
  type?: any;
  size?: any;
  onClick?: any;
  children: any;
  //   startIcon?: React.ReactNode;
  color?: 'inherit' | 'primary' | 'secondary';
  fullWidth?: boolean;
  disabled?: boolean;
  className?: string;
}

const Button = (props: ButtonProps) => {
  const {
    children,
    color,
    onClick,
    // startIcon,
    fullWidth,
    size,
    type,
    disabled,
    className,
  } = props;
  const bgColor =
    color === 'primary'
      ? 'bg-primary text-white'
      : color === 'secondary'
      ? 'bg-secondary text-white'
      : '';
  console.log(size);

  return (
    <button
      className={`${className} ${bgColor} py-2 px-4 ${
        fullWidth ? 'w-full' : ''
      }`}
      type={type}
      color={color}
      disabled={disabled}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

export default Button;
