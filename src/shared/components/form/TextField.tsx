import React from 'react';
import { PropTypes, TextField as MaterialUITextField } from '@material-ui/core';

export enum TextFieldType {
  text = 'text',
  email = 'email',
  tel = 'tel',
  number = 'number',
  password = 'password'
}

interface TextFieldProps {
  fullWidth?: boolean;
  touched?: boolean;
  error?: boolean;
  onBlur?: any;
  onChange?: any;
  label: string;
  name: string;
  value: any;
  variant: any;
  type: TextFieldType;
  helperText?: string | false;
  margin?: PropTypes.Margin;
  required?: boolean;
}

const TextField = (props: TextFieldProps) => {
  const {
    label,
    name,
    value,
    type,
    margin,
    helperText,
    touched,
    error,
    onBlur,
    onChange,
    variant,
    fullWidth,
    required,
  } = props;

  return (
    <MaterialUITextField
      required={required}
      fullWidth={fullWidth}
      error={Boolean(touched && error)}
      helperText={helperText}
      label={label}
      margin={margin}
      name={name}
      onBlur={onBlur}
      onChange={onChange}
      type={type}
      value={value}
      variant={variant}
    />
  );
};

export default TextField;
