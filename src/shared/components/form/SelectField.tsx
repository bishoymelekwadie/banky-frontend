import React from 'react';
import { PropTypes, TextField as MaterialUITextField } from '@material-ui/core';

interface SelectFieldProps {
  fullWidth?: boolean;
  touched?: boolean;
  error?: boolean;
  onBlur?: any;
  onChange?: any;
  label: string;
  name: string;
  value: any;
  variant: any;
  helperText?: string | false;
  margin?: PropTypes.Margin;
  children?: any;
  required?: boolean;
  SelectProps?: any;
}

const SelectField = (props: SelectFieldProps) => {
  const {
    label,
    name,
    value,
    margin,
    helperText,
    touched,
    error,
    onBlur,
    onChange,
    variant,
    fullWidth,
    children,
    required,
    SelectProps,
  } = props;

  return (
    <MaterialUITextField
      select
      SelectProps={SelectProps}
      required={required}
      fullWidth={fullWidth}
      error={Boolean(touched && error)}
      helperText={helperText}
      label={label}
      margin={margin}
      name={name}
      onBlur={onBlur}
      onChange={onChange}
      value={value}
      variant={variant}
    >
      {children}
    </MaterialUITextField>
  );
};

export default SelectField;
