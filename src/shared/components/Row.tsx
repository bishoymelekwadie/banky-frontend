import React from 'react';
import { Grid } from '@material-ui/core';

interface RowProps {
  children: any;
  spacing?: any;
  className?: string;
}

const Row = (props: RowProps) => {
  const { children, spacing, className = '' } = props;
  return (
    <Grid container spacing={spacing} className={`${className}`}>
      {children}
    </Grid>
  );
};

export default Row;
