import React, { forwardRef } from 'react';
import { Helmet } from 'react-helmet';

const Page = forwardRef(
  ({ children, title = '', className, ...rest }: any, ref: any) => {
    return (
      // @ts-ignore
      <div className={`bg-gray-200 h-full ${className}`} ref={ref} {...rest}>
        <Helmet>
          <title>{title}</title>
        </Helmet>
        {children}
      </div>
    );
  }
);

export default Page;
