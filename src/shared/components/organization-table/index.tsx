import { connect } from 'react-redux';
import { StoreReducersList } from 'shared/types';
import {
  GetOrganizationInputProps,
  getOrganization,
} from 'store/organization/actionCreators';
import OrganizationTable from './OrganizationTable';

const mapStateToProps = (state: StoreReducersList) => ({
  userDetails: state.account?.userData,
  isGetOrganizationLoading: state.user.isGetOrganizationLoading,
  organizationList: state.organization.organizationList,
  getOrganizationErr: state.user.getOrganizationErrorMessage,
});

const mapDispatchToProps = (dispatch: any) => ({
  getOrganization: ({
    userId,
    userRole,
    rps,
    rpi,
  }: GetOrganizationInputProps) =>
    dispatch(getOrganization({ userId, userRole, rps, rpi })),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrganizationTable);
