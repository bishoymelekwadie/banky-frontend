import React from 'react';
import { CircularProgress } from '@material-ui/core';

const Spinner = ({ loading }) => {
  if (!loading) return null;

  return (
    <div className="page-spinner">
      <CircularProgress />
      <img alt="Logo" src="/static/logo.svg" />
    </div>
  );
};

export default Spinner;
