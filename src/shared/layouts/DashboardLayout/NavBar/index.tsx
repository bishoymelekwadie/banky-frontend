import React, { useEffect } from 'react';
import { Link as RouterLink, useLocation } from 'react-router-dom';
import {
  Avatar,
  Box,
  Divider,
  Drawer,
  Hidden,
  List,
  Typography,
  makeStyles,
} from '@material-ui/core';
import {
  BarChart as BarChartIcon,
  Settings as SettingsIcon,
  User as UserIcon,
  Users as UsersIcon,
} from 'react-feather';
import { UserRoles } from 'config/permissions';
import NavItem from './NavItem';

const user = {
  avatar: '/static/images/avatars/avatar_1.png',
  jobTitle: 'Account Manager',
  name: 'Ahmed Mahmoud',
};

function getSideMenuNavItems(role: UserRoles) {
  let list = [
    {
      href: '/app/dashboard',
      icon: BarChartIcon,
      title: 'Dashboard',
    },
    {
      href: '/app/cash-in',
      icon: UsersIcon,
      title: 'Cash-In',
    },
    {
      href: '/app/cash-out',
      icon: UsersIcon,
      title: 'Cash-Out',
    },
    {
      href: '/app/transaction',
      icon: UsersIcon,
      title: 'Transactions',
    },
    {
      href: '/app/user',
      icon: UsersIcon,
      title: 'Users',
    },
    {
      href: '/app/organization',
      icon: UsersIcon,
      title: 'Organizations',
    },
    {
      href: '/app/partner',
      icon: UsersIcon,
      title: 'Partners',
    },
    {
      href: '/app/location',
      icon: UsersIcon,
      title: 'Locations',
    },
    {
      href: '/app/account',
      icon: UserIcon,
      title: 'Account',
    },
    {
      href: '/app/settings',
      icon: SettingsIcon,
      title: 'Settings',
    },
  ];

  if (role === UserRoles.Agent) {
    list = list.filter(one =>
      [
        '/app/dashboard',
        '/app/cash-in',
        '/app/cash-out',
        '/app/transaction',
        '/app/account',
      ].includes(one.href)
    );
  }

  if (role === UserRoles.LocationManager) {
    list = list.filter(one =>
      [
        '/app/dashboard',
        '/app/cash-in',
        '/app/cash-out',
        '/app/user',
        '/app/transaction',
        '/app/partner',
        '/app/organization',
        '/app/location',
        '/app/account',
      ].includes(one.href)
    );
  }

  if (role === UserRoles.Partner) {
    list = list.filter(one =>
      [
        '/app/dashboard',
        '/app/user',
        '/app/transaction',
        '/app/location',
        '/app/account',
      ].includes(one.href)
    );
  }
  if (role === UserRoles.OperationManager) {
    list = list.filter(one =>
      [
        '/app/dashboard',
        '/app/user',
        '/app/transaction',
        '/app/location',
        '/app/partner',
        '/app/organization',
        '/app/account',
      ].includes(one.href)
    );
  }
  return list;
}

const useStyles = makeStyles(() => ({
  mobileDrawer: {
    width: 256,
  },
  desktopDrawer: {
    width: 256,
    top: 64,
    height: 'calc(100% - 64px)',
  },
  avatar: {
    cursor: 'pointer',
    width: 64,
    height: 64,
  },
}));

const NavBar = ({
  onMobileClose,
  openMobile,
  role = UserRoles.LocationManager,
}: any) => {
  const items = getSideMenuNavItems(role);
  const classes = useStyles();
  const location = useLocation();

  useEffect(() => {
    if (openMobile && onMobileClose) {
      onMobileClose();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location.pathname]);

  const content = (
    <Box height="100%" display="flex" flexDirection="column">
      <Box alignItems="center" display="flex" flexDirection="column" p={2}>
        <Avatar
          className={classes.avatar}
          component={RouterLink}
          src={user.avatar}
          to="/app/account"
        />
        <Typography color="textPrimary" variant="h5">
          {user.name}
        </Typography>
        <Typography color="textSecondary" variant="body2">
          {user.jobTitle}
        </Typography>
      </Box>
      <Divider />
      <Box p={2}>
        <List>
          {items.map(item => (
            <NavItem
              href={item.href}
              key={item.title}
              title={item.title}
              icon={item.icon}
            />
          ))}
        </List>
      </Box>
    </Box>
  );

  return (
    <>
      <Hidden lgUp>
        <Drawer
          anchor="left"
          classes={{ paper: classes.mobileDrawer }}
          onClose={onMobileClose}
          open={openMobile}
          variant="temporary"
        >
          {content}
        </Drawer>
      </Hidden>
      <Hidden mdDown>
        <Drawer
          anchor="left"
          classes={{ paper: classes.desktopDrawer }}
          open
          variant="persistent"
        >
          {content}
        </Drawer>
      </Hidden>
    </>
  );
};

export default NavBar;
