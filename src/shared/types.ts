import { Transaction } from 'store/api';

export interface Wallet {
  Name: string;
  Logo_Picture_Link: string;
  Wallet_ID: string;
  Corporate_Address: string;
  Corporate_Phone: string;
  Corporate_Email: string;
  Commercial_Contact_Name: string;
  Commercial_Contact_Phone: string;
  Commercial_Contact_Email: string;
  Super_Agent_Limit: string;
  Settlement_Cycle_In_Days: number;
  Daily_Closing_Time: string;
}

export interface WalletTypeOption {
  id: string;
  label: string;
}

export interface Location {
  Location_Name: string;
  Location_ID: string;
  Address: string;
  Geo_Location: string;
  Partner_ID: string;
  Location_Phone: string;
  Limit: string;
}

export interface User {
  Full_Name: string;
  Phone_Number: string;
  Profile_Picture_Link: string;
  Email_Address: string;
  UserName: string;
  Password: string;
  Organization_ID: string;
  Organization_Type: string;
  Role: string;
  Scope: string;
}

export interface CioPartner {
  Partner_Name: string;
  Partner_Logo_Picture_Link: string;
  Partner_ID: string;
  Corporate_Address: string;
  Corporate_Phone: string;
  Corporate_Email: string;
  Commercial_Contact_Name: string;
  Commercial_Contact_Phone: string;
  Commercial_Contact_Email: string;
  Limit: string;
  Settlement_Cycle_In_Days: string;
  Daily_Closing_Time: string;
}

export interface CredentialsProps {
  email: string;
  password: string;
}

/**
 * ******************************
 * ******** Stores **************
 * ******************************
 */
export interface StoreReducersList {
  wallet: any;
  account: any;
  location: any;
  transaction: any;
  user: any;
  organization: any;
  partner: any;
}

export interface WalletStoreState {
  availableTypes: WalletTypeOption[] | [];
  isLoading: Boolean;
  errorMessage: string | undefined;
}

export interface CashInInputProps {
  amount: number;
  created_by_id: number;
  fees: number;
  location_id: number;
  notes: string;
  partner_org_id: number;
  status: Transaction.StatusEnum;
  wallet_org_id: number;
  wallet_phone: string;
  type: Transaction.TypeEnum;
}

export interface CashOutInputProps {
  transactionType: string;
  walletType: any;
  walletNumber: string;
  amount: number;
  // TODO:should be number
  pin: string;
}

export interface PaginationSuccessProps {
  list?: Location[];
  pageIndex?: number;
  pageSize?: number;
  totalCount?: number;
}
