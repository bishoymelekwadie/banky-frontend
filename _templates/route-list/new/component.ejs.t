---
to: src/pages/<%= name %>-list/List<%= Name %>.tsx
---
import React from 'react';
import { Container } from '@material-ui/core';
import Page from 'shared/components/Page';
import Row from 'shared/components/Row';
import Col from 'shared/components/Col';
import <%= Name %>Table from 'shared/components/<%= name %>-table';
import Spinner from 'shared/components/spinner';

interface List<%= Name %>Props {
  isLoading: boolean;
}

const List<%= Name %> = (props: List<%= Name %>Props) => {
  const { isLoading } = props;

  return (
    <Page className="mt-8" title="<%= Name %> List">
      <Spinner loading={isLoading} />
      <Container maxWidth={false}>
        <Row>
          <Col className="mt-4" xs={12}>
            < <%= Name %>Table />
          </Col>
        </Row>
      </Container>
    </Page>
  );
};

export default List<%= Name %>;
