---
to: src/shared/components/<%= name %>-table/<%= Name %>Table.tsx
---
import React, { useEffect, useState } from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Card, CardHeader } from '@material-ui/core';
import { $TYPE$ } from 'shared/types';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import { DataGrid } from '@material-ui/data-grid';
import IconButton from '@material-ui/core/IconButton';
import CreateIcon from '@material-ui/icons/Create';

interface <%= Name %>ListProps {
  <%= name %>List?: $TYPE$;
  get<%= Name %>?: any;
  userDetails: any;
}

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'name', headerName: 'Name', width: 180 },
  { field: 'status', headerName: 'Status', width: 130 },
  {
    field: 'daily_limit',
    headerName: 'Daily Limit',
    sortable: false,
    width: 160,
  },
  { field: 'address', headerName: 'Address', width: 300 },
];

const <%= Name %>ListTable = (props: <%= Name %>ListProps) => {
  const { <%= name %>List = [], userDetails, get<%= Name %> } = props;
  const navigate = useNavigate();
  const { t } = useTranslation();
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(10);

  useEffect(() => {
    get<%= Name %>({
      userId: userDetails?.id,
      userRole: userDetails?.roles,
      rpi: page + 1,
      rps: pageSize,
    });
  }, [page, pageSize]);

  return (
    <Card>
      <CardHeader
        title={t('<%= name %>.list')}
        action={
          <IconButton
            onClick={() => navigate('/app/<%= name %>/create')}
            aria-label="create"
          >
            <CreateIcon />
          </IconButton>
        }
      />
      <PerfectScrollbar>
        <div style={{ height: 500, width: '100%' }}>
          <DataGrid
            rows={<%= name %>List}
            columns={columns}
            checkboxSelection={false}
            rowsPerPageOptions={[20, 50]}
            page={page}
            onPageChange={(params: any) => setPage(params.page)}
            onPageSizeChange={(params: any) => setPageSize(params.pageSize)}
            pageSize={pageSize}
          />
        </div>
      </PerfectScrollbar>
    </Card>
  );
};

export default <%= Name %>ListTable;
