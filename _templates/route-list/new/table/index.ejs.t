---
to: src/shared/components/<%= name %>-table/index.tsx
---
import { connect } from 'react-redux';
import { StoreReducersList } from 'shared/types';
import {
  Get<%= Name %>InputProps,
  get<%= Name %>,
} from 'store/<%= name %>/actionCreators';
import <%= Name %>Table from './<%= Name %>Table';

const mapStateToProps = (state: StoreReducersList) => ({
  userDetails: state.account?.userData,
  isGet<%= Name %>Loading: state.user.isget<%= Name %>Loading,
  <%= name %>List: state.<%= name %>.<%= name %>List,
  get<%= Name %>Err: state.user.get<%= Name %>ErrorMessage,
});

const mapDispatchToProps = (dispatch: any) => ({
  get<%= Name %>: ({ userId, userRole, rps, rpi }: Get<%= Name %>InputProps) =>
    dispatch(get<%= Name %>({ userId, userRole, rps, rpi })),
});

export default connect(mapStateToProps, mapDispatchToProps)(<%= Name %>Table);
