---
to: src/pages/<%= name %>-list/index.ts
---
import { connect } from 'react-redux';
import { StoreReducersList } from 'shared/types';
import List<%= Name %> from './List<%= Name %>';

const mapStateToProps = (state: StoreReducersList) => ({
  isLoading: state.<%= name %>.isGetListLoading,
});

export default connect(mapStateToProps)(List<%= Name %>);
