---
to: src/pages/<%= name %>-create/index.ts
---
import { connect } from 'react-redux';
import { StoreReducersList } from 'shared/types';
import { createItem, cleanStore } from 'store/<%= name %>/actionCreators';
import Create<%= Name %> from './Create<%= Name %>';

const mapStateToProps = (state: StoreReducersList) => ({
  userDetails: state.account.userData,
  isLoading: state.<%= name %>.isCreateItemLoading,
  createdItem: state.<%= name %>.createdItem,
  createItemErr: state.<%= name %>.createItemErr,
});

export default connect(mapStateToProps, (dispatch: any) => ({
  createItem: ({ userId, userRole, item }: any) =>
    dispatch(createItem({ userId, userRole, item })),
    cleanStore: () => dispatch(cleanStore()),
}))(
  Create<%= Name %>
);
