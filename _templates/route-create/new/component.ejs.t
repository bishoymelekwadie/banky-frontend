---
to: src/pages/<%= name %>-create/Create<%= Name %>.tsx
---
import React from 'react';
import { useNavigate } from 'react-router-dom';
import {
  Card,
  CardHeader,
  Container,
  Divider,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import * as Yup from 'yup';
import { Formik } from 'formik';
import Page from 'shared/components/Page';
import Row from 'shared/components/Row';
import Col from 'shared/components/Col';
import TextField, { TextFieldType } from 'shared/components/form/TextField';
import Alert, { AlertTypes } from 'shared/components/Alert';
import Button from 'shared/components/Button';
import Spinner from 'shared/components/spinner';

interface Create<%= Name %>Props {
  createdItem: any;
  createItemErr: string;
  userDetails: any;
  isLoading: boolean;
  createItem: any;
}

const Create<%= Name %> = (props: Create<%= Name %>Props) => {
  const { createdItem,createItem, createItemErr, userDetails, isLoading } = props;
  const { t } = useTranslation();
  const navigate = useNavigate();

  return (
    <Page className="mt-8" title={t('<%= name %>.create.title')}>
      <Spinner loading={isLoading} />
      <Container maxWidth={false}>
        <Row>
          <Col className="mt-4" xs={12}>
            <Card>
              <CardHeader title={t('<%= name %>.create.title')} />
              {createItemErr && (
                <Alert className="my-8" type={AlertTypes.Error}>
                  {createItemErr}
                </Alert>
              )}
              <Formik
                initialValues={{
                  %FF%: '',
                }}
                validationSchema={Yup.object().shape({
                  %FF%: Yup.string()
                    .max(255)
                    .required('Email is required'),
                })}
                onSubmit={item =>
                  createItem({
                    item,
                    userId: userDetails?.id,
                    userRole: userDetails?.roles,
                  })}
              >
                {({
                  errors,
                  handleBlur,
                  handleChange,
                  handleSubmit,
                  isSubmitting,
                  touched,
                  values,
                }) => (
                  <form onSubmit={handleSubmit} className="mx-4">
                    <Row>
                      <Col className="mt-4" xs={12} md={6}>
                        <TextField
                          fullWidth
                          name="name"
                          margin="normal"
                          label={t('<%= name %>.create.locationName')}
                          value={values.name}
                          touched={touched.name}
                          error={Boolean(touched.name && errors.name)}
                          helperText={touched.name && errors.name}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          type={TextFieldType.text}
                          variant="outlined"
                        />
                      </Col>
                      <Col className="mt-4" xs={12} md={6}>
                        <TextField
                          fullWidth
                          label={t('<%= name %>.create.dailyLimit')}
                          margin="normal"
                          name="daily_limit"
                          variant="outlined"
                          value={values.daily_limit}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          type={TextFieldType.number}
                          helperText={touched.daily_limit && errors.daily_limit}
                          error={Boolean(
                            touched.daily_limit && errors.daily_limit
                          )}
                        />
                      </Col>
                      <Divider flexItem />
                    </Row>
                     <div className="flex flex-row-reverse mt-10 mb-4">
                      <Button
                        className="mx-2"
                        color="primary"
                        size="large"
                        type="submit"
                      >
                        {t('<%= name %>.create.createBtn')}
                      </Button>
                      <Button
                        onClick={() => navigate(-1)}
                        className="mx-2"
                        color="secondary"
                        size="large"
                        type="submit"
                      >
                        {t('navigation.cancel')}
                      </Button>
                    </div>            
                  </form>
                )}
              </Formik>
            </Card>
          </Col>
        </Row>
      </Container>
    </Page>
  );
};

export default Create<%= Name %>;
