---
to: src/store/<%= name %>/actionCreators.ts
---
/* eslint-disable no-nested-ternary */
/* eslint-disable no-return-await */
import { PaginationSuccessProps } from 'shared/types';
import { <%= Name %>Api } from 'store/api';

const <%= name %>Api = new <%= Name %>Api({ basePath: process.env.API_URL });

export const <%= name %>Actions = {
  CLEAN_STORE: '<%= NAME %>/CLEAN_STORE',
  GET_LIST: '<%= NAME %>/GET_LIST',
  GET_LIST_START_LOADING: '<%= NAME %>/GET_LIST_START_LOADING',
  GET_LIST_FAILED: '<%= NAME %>/GET_LIST_FAILED',
  GET_LIST_SUCCEED: '<%= NAME %>/GET_LIST_SUCCEED',
  CREATE_ITEM: '<%= NAME %>/CREATE_ITEM',
  CREATE_ITEM_START_LOADING: '<%= NAME %>/CREATE_ITEM_START_LOADING',
  CREATE_ITEM_FAILED: '<%= NAME %>/CREATE_ITEM_FAILED',
  CREATE_ITEM_SUCCEED: '<%= NAME %>/CREATE_ITEM_SUCCEED',
};


/**
 * clean store
 */
export const cleanStore = () => ({
  type: <%= name %>Actions.CLEAN_STORE,
});


/**
 * Get <%= name %>s List
 */
export const startLoadingGetList = () => ({
  type: <%= name %>Actions.GET_LIST_START_LOADING,
});

export const get<%= Name %>Succeed = ({
  list,
  pageIndex,
  pageSize,
  totalCount,
}: PaginationSuccessProps) => ({
  type: <%= name %>Actions.GET_LIST_SUCCEED,
  list,
  pageIndex,
  pageSize,
  totalCount,
});

export const get<%= Name %>Failed = (errMessage: any, meta?: any) => ({
  type: <%= name %>Actions.GET_LIST_FAILED,
  errMessage,
  meta,
});

export interface Get<%= Name %>InputProps {
  userId: number;
  userRole?: string;
  rps?: number;
  rpi?: number;
}

export function get<%= Name %>({
  userId,
  userRole = '2',
  rps,
  rpi,
}: Get<%= Name %>InputProps) {
  return async (dispatch: any) => {
    try {
      dispatch(startLoadingGetList());
      const res = await <%= name %>Api.getAll<%= Name %>(
        23,
        undefined,
        rps,
        rpi,
        undefined,
        { headers: { accept: 'application/json' } }
      );

      const { results, pageIndex, pageSize, totalCount } = res;

      dispatch(
        get<%= Name %>Succeed({
          list: results,
          pageIndex,
          pageSize,
          totalCount,
        })
      );
    } catch (error) {
      console.error(error);
      dispatch(
        get<%= Name %>Failed('Something went wrong,please check your credentials')
      );
    }
  };
}

/**
 * Create new <%= name %>
 */
export const startLoadingCreateItem = () => ({
  type: <%= name %>Actions.CREATE_ITEM_START_LOADING,
});

export const createItemSucceed = ({ item, message }: any) => ({
  type: <%= name %>Actions.CREATE_ITEM_SUCCEED,
  item,
  message,
});

export const createItemFailed = (errMessage: any, meta?: any) => ({
  type: <%= name %>Actions.CREATE_ITEM_FAILED,
  errMessage,
  meta,
});

export interface CreateItemInputProps {
  userId: number;
  userRole?: string;
  item?: any;
}

export function createItem({
  userId,
  userRole = '2',
  item: inputItem,
}: CreateItemInputProps) {
  return async (dispatch: any) => {
    try {
      dispatch(startLoadingCreateItem());
      const data: any = await <%= name %>Api.create<%= Name %>(
        userId,
        inputItem,
        undefined,
        {
          headers: { accept: 'application/json' },
        }
      );

      if (data)
        dispatch(
          createItemSucceed({
            item: data,
            message: 'Item Created',
          })
        );
      else dispatch(createItemFailed('Something went wrong'));
    } catch (error) {
      console.error(error);
      dispatch(
        createItemFailed('Something went wrong,please check your credentials')
      );
    }
  };
}
