---
to: src/store/<%= name %>/index.ts
---
/* eslint-disable no-param-reassign */
import produce from 'immer';
import { <%= name %>Actions } from './actionCreators';

/**
 * Initial State for the Data resource (Service Providers)
 */
const <%= name %>InitialState = {
  isGetListLoading: false,
  <%= name %>List: [],
  get<%= Name %>ErrorMessage: undefined,
};

const <%= name %>Reducer = (state = <%= name %>InitialState, action: any) =>
  produce(state, (draft: any) => {
    switch (action.type) {
      /**
       * ***********************************
       * get-<%= name %> handlers
       * ***********************************
       */
      case <%= NAME %>Actions.GET_LIST_START_LOADING:
        draft.get<%= Name %>ErrorMessage = undefined;
        draft.isGetListLoading = true;
        break;
      case <%= NAME %>Actions.GET_LIST_SUCCEED:
        draft.<%= name %>List = action?.list;
        draft.pageSize = action?.pageSize;
        draft.pageIndex = action?.pageIndex;
        draft.totalCount = action?.totalCount;
        draft.isGetListLoading = false;
        break;
      case <%= NAME %>Actions.GET_LIST_FAILED:
        draft.get<%= Name %>ErrorMessage = action?.errMessage;
        draft.isGetListLoading = false;
        break;

      /**
       * ***********************************
       * create-item handlers
       * ***********************************
       */
      case <%= NAME %>Actions.CREATE_ITEM_START_LOADING:
        draft.createItemErrorMessage = undefined;
        draft.isCreateItemLoading = true;
        break;
      case <%= NAME %>Actions.CREATE_ITEM_SUCCEED:
        draft.item = action?.item;
        draft.isCreateItemLoading = false;
        break;
      case <%= NAME %>Actions.CREATE_ITEM_FAILED:
        draft.createItemErrorMessage = action?.errMessage;
        draft.isCreateItemLoading = false;
        break;
      case <%= NAME %>Actions.CLEAN_STORE:
        draft = <%= name %>InitialState;
        break;

      default:
        break;
    }
    return draft;
  });

export default <%= name %>Reducer;
